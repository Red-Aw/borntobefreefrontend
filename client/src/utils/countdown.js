export const countdown = (countDownTimestamp) => {
    if (!Number.isInteger(countDownTimestamp)) {
        return 'Unknown timestamp';
    }

    let currentTimestamp = Math.round(Date.now() / 1000);

    let distance = countDownTimestamp - currentTimestamp;

    if (distance < 0) {
        return 'Expired';
    }

    let days = Math.floor(distance / (60 * 60 * 24));
    let hours = Math.floor((distance % (60 * 60 * 24)) / (60 * 60));
    let minutes = Math.floor((distance % (60 * 60)) / 60);
    let seconds = Math.floor(distance % 60);

    return days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's';
}
