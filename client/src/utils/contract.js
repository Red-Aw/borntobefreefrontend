import { ethers, Contract } from 'ethers';
import BornToBeFreeToken from '../abi/BornToBeFreeToken.json';
import { connectToWallet, setIsWalletLoading } from '../redux/slices/walletSlice';
import { connectToContract, setIsContractLoading, setPancakeSwapPrice } from '../redux/slices/contractSlice';
import { setIsRequestToBlockchain } from '../redux/slices/blockchainSlice';
import store from '../redux/store';
import SnackbarUtils from './snackbar'

const MessageList = {
    installMetamask: 'Install MetaMask!',
    problemConnectingToMetamask: 'There was a problem connecting to MetaMask',
    connectedSuccessfully: 'Connected successfully!',
    problemConnectingToBlockchain: 'There was a problem connecting to blockchain',
    transactionFailed: 'Transaction failed!',
    failedToGetTokenPrice: 'Failed to get token price!',
};

function getFormattedAmount(providedAmount) {
    const amount = providedAmount.toString()
    if (amount === '0' || amount === '0.0') {
        return '0';
    }

    let formattedEther = ethers.formatEther(amount);
    let strLength = formattedEther.length;
    let strLastIndex = formattedEther.lastIndexOf('.') + 3;
    let slicePoint = strLastIndex;
    if (strLastIndex > strLength) {
        slicePoint = strLength;
    }

    return formattedEther.slice(0, slicePoint)
}

function prepareVestedAmounts(vestedAmounts) {
    let lockedAmount = BigInt(0);
    let amountReadyToBeUnlocked = BigInt(0);
    let nextUnlockTimestamp = 0;

    let blockchainTimestamp = Math.round(Date.now() / 1000) - 180; // minus 3 minutes

    vestedAmounts.forEach(vestedType => {
        vestedType.forEach(lockedStructure => {
            if (lockedStructure.amount === 0) {
                return;
            }

            if (nextUnlockTimestamp === 0 && lockedStructure.releaseTimestamp > blockchainTimestamp) {
                nextUnlockTimestamp = parseInt(lockedStructure.releaseTimestamp)
            }

            if (lockedStructure.releaseTimestamp > blockchainTimestamp) {
                lockedAmount += BigInt(lockedStructure.amount);
            } else {
                amountReadyToBeUnlocked += BigInt(lockedStructure.amount);
            }
        })
    });

    return {
        'lockedAmount': lockedAmount,
        'amountReadyToBeUnlocked': amountReadyToBeUnlocked,
        'nextUnlockTimestamp': nextUnlockTimestamp,
    };
};

async function getAndSetTokenPrice() {
    fetch('https://api.coinbrain.com/public/coin-info', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "56": [ // chainId. TODO: make this better by getting data from env?
                "0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c", // contract. TODO: make this better by getting data from env?
            ]
        }),
    })
    .then((response) => response.json())
    .then((data) => {
        store.dispatch(
            setPancakeSwapPrice(data[0].priceUsd)
        );
    })
    .catch((err) => {
        SnackbarUtils.error(MessageList.failedToGetTokenPrice);
        console.log(err.message);
    });
}

const getBlockchain = async () => {
    if (window.ethereum) {
        await window.ethereum.request({ method: 'eth_requestAccounts' });

        const provider = new ethers.BrowserProvider(window.ethereum);
        if (!provider) {
            SnackbarUtils.error(MessageList.installMetamask);
            return;
        }

        const signer = await provider.getSigner();
        const signerAddress = await signer.getAddress();

        const bornToBeFreeContract = new Contract(
            (process.env.NEXT_PUBLIC_BTBF_CONTRACT_ADDRESS).toString(),
            BornToBeFreeToken.abi,
            signer,
        );

        return {
            'contractInstance': bornToBeFreeContract,
            'signerAddress': signerAddress,
        };
    } else {
        store.dispatch(setIsRequestToBlockchain(false));
        store.dispatch(setIsWalletLoading(false));
        store.dispatch(setIsContractLoading(false));
        SnackbarUtils.error(MessageList.installMetamask);
        return;
    }
};

const prepareStoreValues = async (contractInstance, signerAddress) => {
    try {
        const totalSupply = await contractInstance.totalSupply();
        const burntTokenCount = await contractInstance.totalBurnAmount();
        const sellTaxPercentage = await contractInstance.sellTaxPercentage();
        const buyTaxPercentage = await contractInstance.buyTaxPercentage();
        const burnPercentage = await contractInstance.burnPercentage();
        const getBalanceAccounts = await contractInstance.getBalanceAccounts();

        let totalSupplyAfterBurn = BigInt(totalSupply);
        let circulatingSupply = totalSupplyAfterBurn - BigInt('4000000000000000000000000');
        store.dispatch(
            connectToContract({
                maxSupply: getFormattedAmount(totalSupply),
                totalSupply: getFormattedAmount(totalSupplyAfterBurn),
                burntTokenCount: getFormattedAmount(burntTokenCount),
                circulatingSupply: getFormattedAmount(circulatingSupply),
                sellTaxPercentage: parseInt(sellTaxPercentage),
                buyTaxPercentage: parseInt(buyTaxPercentage),
                burnPercentage: parseInt(burnPercentage),
                totalWalletCount: parseInt(getBalanceAccounts.length),
            })
        );

        const balance = await contractInstance.balanceOf(signerAddress);
        const lockedBalances = await contractInstance.getDetailedLockedBalanceOf(signerAddress);
        const vestedAmounts = prepareVestedAmounts(lockedBalances);

        store.dispatch(
            connectToWallet({
                publicKey: signerAddress,
                balance: getFormattedAmount(balance),
                lockedAmount: getFormattedAmount(vestedAmounts.lockedAmount),
                isAmountReadyToBeUnlocked: parseFloat(getFormattedAmount(vestedAmounts.amountReadyToBeUnlocked)) > 0 ? true : false,
                nextUnlockTimestamp: vestedAmounts.nextUnlockTimestamp,
            })
        );

        store.dispatch(setIsRequestToBlockchain(false));
        store.dispatch(setIsWalletLoading(false));
        store.dispatch(setIsContractLoading(false));

        SnackbarUtils.success(MessageList.connectedSuccessfully);
    } catch (error) {
        SnackbarUtils.error(MessageList.problemConnectingToMetamask);
        return;
    }
};

export const prepareContractValues = async () => {
    const state = store.getState();
    if (state.blockchain.isRequestToBlockchain) {
        return;
    }
    if (state.blockchain.isDisconnect) {
        return;
    }
    store.dispatch(setIsRequestToBlockchain(true));
    store.dispatch(setIsWalletLoading(true));
    store.dispatch(setIsContractLoading(true));

    const blockchain = await getBlockchain();
    if (blockchain === undefined) {
        SnackbarUtils.error(MessageList.problemConnectingToBlockchain);
        return;
    }

    const signerAddress = blockchain.signerAddress;
    const contractInstance = blockchain.contractInstance;
    if (contractInstance === undefined || signerAddress === undefined) {
        SnackbarUtils.error(MessageList.problemConnectingToBlockchain);
        return;
    }

    await prepareStoreValues(contractInstance, signerAddress);
    await getAndSetTokenPrice();
};

export const releaseVestedAmounts = async () => {
    const state = store.getState();
    if (state.blockchain.isRequestToBlockchain) {
        return;
    }
    store.dispatch(setIsRequestToBlockchain(true));
    store.dispatch(setIsWalletLoading(true));
    store.dispatch(setIsContractLoading(true));

    const blockchain = await getBlockchain();
    if (blockchain === undefined) {
        SnackbarUtils.error(MessageList.problemConnectingToBlockchain);
        return;
    }

    const signerAddress = blockchain.signerAddress;
    const contractInstance = blockchain.contractInstance;
    if (contractInstance === undefined || signerAddress === undefined) {
        SnackbarUtils.error(MessageList.problemConnectingToBlockchain);
        return;
    }

    try {
        await contractInstance.releaseAmountsFor(
            signerAddress,
            {
                gasLimit: 90000,
            }
        );
    } catch (error) {
        store.dispatch(setIsRequestToBlockchain(false));
        store.dispatch(setIsWalletLoading(false));
        store.dispatch(setIsContractLoading(false));
        SnackbarUtils.error(MessageList.transactionFailed);
        return;
    }
    await prepareStoreValues(contractInstance, signerAddress);
};
