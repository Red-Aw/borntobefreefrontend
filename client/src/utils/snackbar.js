import { useSnackbar } from 'notistack'
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import React, { Fragment } from 'react';

let useSnackbarRef;
export const SnackbarUtilsConfigurator = () => {
    useSnackbarRef = useSnackbar()

    return null
}

const action = key => (
    <Fragment>
        <IconButton onClick={() => { useSnackbarRef.closeSnackbar(key) }}>
            <CloseIcon
                sx={{
                    color: 'neutral.200',
                }}
            />
        </IconButton>
    </Fragment>
);

export default {
    success(msg) {
        this.toast(msg, 'success')
    },
    warning(msg) {
        this.toast(msg, 'warning')
    },
    info(msg) {
        this.toast(msg, 'info')
    },
    error(msg) {
        console.error(msg);
        this.toast(msg, 'error', null)
    },
    toast(msg, variant = 'default', duration = 6000) {
        useSnackbarRef.enqueueSnackbar(msg, { variant, autoHideDuration: duration, action })
    }
}