import NextLink from 'next/link';
import styled from '@emotion/styled';
import { Container, AppBar } from '@mui/material';

const StyledContainer = styled(Container)(({ theme }) => ({
    backgroundColor: theme.palette.background.blueShadow,
    boxShadow: theme.shadows[6]
}));

const Background = styled('div')(() => ({
    backgroundImage: "url('/static/images/background.jpg')",
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    height: '100vh',
    width: '100vw',
    overflowY: 'scroll',
    overflowX: 'hidden',
}));

const DashboardLayoutRoot = styled('div')(() => ({
    display: 'flex',
    flex: '1 1 auto',
    maxWidth: '100%',
}));

const DashboardNavbarRoot = styled(AppBar)(({ theme }) => ({
    boxShadow: theme.shadows[0]
}));

const StyledNextLink = styled(NextLink)(() => ({
    textTransform: 'none',
    textDecoration: 'none'
}));

export { StyledContainer, Background, DashboardLayoutRoot, DashboardNavbarRoot, StyledNextLink };