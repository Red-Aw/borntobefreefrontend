import {
    Box,
    Typography,
    Stack,
    List,
} from '@mui/material';
import { NavItem } from './NavItem';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';

const SiteList = [
    {
        id: 'whyCrypto',
        href: '/whyCrypto',
        title: 'Why Crypto'
    },
    {
        id: 'howDoesItWork',
        href: '/howDoesItWork',
        title: 'How Does It Work'
    },
    {
        id: 'faq',
        href: '/faq',
        title: 'FAQ'
    },
];

const SocialMediaList = [
    {
        id: 'facebook',
        href: '/fb', // TODO: fix link
        title: '',
        icon: <FacebookIcon size='small'/>
    },
    {
        id: 'instagram',
        href: '/ig', // TODO: fix link
        title: '',
        icon: <InstagramIcon size='small'/>
    },
];

const AuditList = [
    {
        id: 'audit',
        href: '/audit', // TODO: fix link
        title: 'Audit'
    },
];

export const DashboardFooter = () => {
    return (
        <Box
            sx={{
                backgroundColor: 'background.dark',
                py: 2,
            }}
        >
            <Stack
                direction='row'
                justifyContent='space-evenly'
                spacing={2}
            >
                <List
                    sx={{
                        display: 'flex',
                        alignItems: 'center',
                    }}
                >
                    {SiteList.map((item) => (
                        <NavItem
                            key={item.id}
                            icon={item.icon}
                            href={item.href}
                            title={item.title}
                        />
                    ))}
                </List>

                <List
                    sx={{
                        display: 'flex',
                        alignItems: 'center',
                    }}
                >
                    <Typography
                        sx={{
                            color: 'neutral.500',
                            fontWeight: 500,
                        }}
                    >
                        Join Us
                    </Typography>

                    {SocialMediaList.map((item) => (
                        <NavItem
                            key={item.id}
                            icon={item.icon}
                            href={item.href}
                            title={item.title}
                        />
                    ))}
                </List>

                <List
                    sx={{
                        display: 'flex',
                        alignItems: 'center',
                    }}
                >
                    {AuditList.map((item) => (
                        <NavItem
                            key={item.id}
                            icon={item.icon}
                            href={item.href}
                            title={item.title}
                        />
                    ))}
                </List>
            </Stack>
        </Box>
    );
};