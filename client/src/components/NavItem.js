import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Typography, IconButton } from '@mui/material';
import { StyledNextLink } from '../utils/styledComponents';

export const NavItem = (props) => {
    const { href, icon, title, ...others } = props;
    const router = useRouter();
    const active = href ? (router.pathname === href) : false;

    const borderBottom = '4px solid #EDB52D';

    return (
        <StyledNextLink
            href={ href }
            passHref
            className='StyledNextLink'
        >
            {icon && !title &&
                <IconButton
                    sx={{
                        color: 'neutral.500',
                        '&:hover': {
                            color: 'neutral.200',
                            backgroundColor: 'background.dark'
                        }
                    }}
                >
                    { icon }
                </IconButton>
            }

            {!icon && title &&
                <Typography
                    sx={{
                        color: active ? 'neutral.200' : 'neutral.500',
                        borderBottom: active ? borderBottom : 'none',
                        '&:hover': {
                            color: 'neutral.200',
                            borderBottom: borderBottom,
                        },
                        mx: 3,
                        my: 0,
                        textAlign: 'center',
                    }}
                >
                    { title }
                </Typography>
            }
        </StyledNextLink>
    );
};

NavItem.propTypes = {
    href: PropTypes.string,
    icon: PropTypes.node,
    title: PropTypes.string
};
