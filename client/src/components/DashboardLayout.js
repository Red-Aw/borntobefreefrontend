import { Box } from '@mui/material';
import DashboardNavbar from './DashboardNavbar';
import { DashboardFooter } from './DashboardFooter';
import { Background, DashboardLayoutRoot } from '../utils/styledComponents';
import dynamic from 'next/dynamic';
const MouseParticles = dynamic(() => import('react-mouse-particles'), { ssr: false });

export const DashboardLayout = (props) => {
    const { children } = props;

    return (
        <Background>
            <DashboardNavbar />

            <MouseParticles
                g={1}
                color='#EDB52D'
                cull='MuiButton-root,MuiSvgIcon-root,StyledNextLink'
                level={2}
            />

            <DashboardLayoutRoot>
                <Box
                    sx={{
                        display: 'flex',
                        flex: '1 1 auto',
                        flexDirection: 'column',
                        width: '100%',
                        color: 'neutral.200',
                    }}
                >
                    {children}
                </Box>
            </DashboardLayoutRoot>

            <DashboardFooter />
        </Background>
    );
};
