import React, { useEffect, useState, useRef } from 'react';
import {
    Grow,
    Popper,
    Paper,
    MenuItem,
    MenuList,
    ClickAwayListener,
    ListItemIcon,
    Box,
} from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import { useDispatch, useSelector } from 'react-redux';
import { disconnectFromWallet, setIsWalletLoading } from '../../redux/slices/walletSlice';
import { disconnectFromContract } from '../../redux/slices/contractSlice';
import { setIsDisconnect } from '../../redux/slices/blockchainSlice';
import SnackbarUtils from '../../utils/snackbar';
import { prepareContractValues } from '../../utils/contract.js';

export const ConnectWallet = () => {
    const chainChanged = 'Chain changed. Please reload the page';

    // Redux
    const walletState = useSelector((state) => state.wallet);
    const dispatch = useDispatch();

    useEffect(() => {
        prepareContractValues();
    });

    // Connect wallet
    const [walletOpen, setWalletOpen] = useState(false);
    const anchorRef = useRef(null);

    const handleToggle = () => {
        setWalletOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }

        setWalletOpen(false);
    };

    function handleListKeyDown(event) {
        if (event.key === 'Tab') {
            event.preventDefault();
            setWalletOpen(false);
        } else if (event.key === 'Escape') {
            setWalletOpen(false);
        }
    }

    const prevOpen = useRef(walletOpen);
    useEffect(() => {
        if (prevOpen.current === true && walletOpen === false) {
            anchorRef.current.focus();
        }

        prevOpen.current = walletOpen;
    }, [walletOpen]);

    // Metamask methods
    useEffect(() => {
        if (window.ethereum) {
            window.ethereum.on('accountsChanged', handleAccountsChanged);
            window.ethereum.on('chainChanged', handleChainChanged);
        }
    }, []);

    const connectMetamask = async () => {
        dispatch(setIsDisconnect(false));
        handleToggle();
        await handleAccountsChanged();
    };

    const handleAccountsChanged = async () => {
        prepareContractValues();
    };

    const handleChainChanged = async () => {
        dispatch(disconnectFromWallet());
        dispatch(disconnectFromContract());
        dispatch(setIsWalletLoading(false));
        SnackbarUtils.error(chainChanged);
    };

    return (
        <>
            <LoadingButton
                ref={ anchorRef }
                id='composition-button'
                aria-controls={walletOpen ? 'composition-menu' : undefined}
                aria-expanded={walletOpen ? 'true' : undefined}
                aria-haspopup='true'
                onClick={ handleToggle }
                size='small'
                loading={ walletState.isLoading }
                sx={{
                    backgroundColor: 'metamask.active',
                    color: 'neutral.100',
                    fontWeight: 700,
                    px: 2,
                    '&:hover': {
                        backgroundColor: 'metamask.hover'
                    },
                }}
            >
                Connect wallet
            </LoadingButton>

            <Popper
                open={ walletOpen }
                anchorEl={ anchorRef.current }
                role={ undefined }
                placement='bottom-start'
                transition
                disablePortal
            >
                {({ TransitionProps, placement }) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin:
                            placement === 'bottom-start' ? 'left top' : 'left bottom',
                        }}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={ handleClose }>
                                <MenuList
                                    autoFocusItem={ walletOpen }
                                    id='composition-menu'
                                    aria-labelledby='composition-button'
                                    onKeyDown={ handleListKeyDown }
                                >
                                    <MenuItem onClick={ connectMetamask }>
                                        <ListItemIcon>
                                            <Box
                                                component='img'
                                                alt='Metamask'
                                                src='/static/images/metamask.svg'
                                                style={{
                                                    display: 'inline-block',
                                                    maxWidth: '100%',
                                                }}
                                            />
                                        </ListItemIcon>
                                        Metamask
                                    </MenuItem>
                                </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </>
    );
};