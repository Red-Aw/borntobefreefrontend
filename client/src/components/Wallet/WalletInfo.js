import React from 'react';
import {
    Menu,
    MenuItem,
    Divider,
    Fade,
} from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import { useDispatch, useSelector } from 'react-redux';
import { disconnectFromWallet, setIsWalletLoading } from '../../redux/slices/walletSlice';
import { disconnectFromContract } from '../../redux/slices/contractSlice';
import { setIsDisconnect } from '../../redux/slices/blockchainSlice';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import ContentCopy from '@mui/icons-material/ContentCopy';
import LogoutIcon from '@mui/icons-material/Logout';
import SnackbarUtils from '../../utils/snackbar';

export const WalletInfo = () => {
    const MessageList = {
        publicKeyCopiedToClipboard: 'Public key copied to clipboard!',
        disconnected: 'Disconnected!',
    };

    // Redux
    const walletState = useSelector((state) => state.wallet);
    const dispatch = useDispatch();

    // wallet settings
    const [anchorEl, setAnchorEl] = React.useState(null);
    const isOpen = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    // Methods
    function getShortenedKey(string) {
        return string.slice(0,4) + '...' + string.slice(-4);
    }

    function copyToClipboard() {
        handleClose();
        navigator.clipboard.writeText(walletState.wallet.publicKey.toString());
        SnackbarUtils.success(MessageList.publicKeyCopiedToClipboard);
    }

    function handleDisconnect() {
        handleClose();

        dispatch(setIsDisconnect(true));
        dispatch(setIsWalletLoading(true));
        dispatch(disconnectFromWallet());
        dispatch(disconnectFromContract());
        dispatch(setIsWalletLoading(false));

        SnackbarUtils.success(MessageList.disconnected);
    }

    return (
        <>
            <LoadingButton
                id="fade-button"
                aria-controls={isOpen ? 'fade-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={isOpen ? 'true' : undefined}
                onClick={ handleClick }
                loading={ walletState.isLoading }
                sx={{
                    color: 'neutral.200',
                    fontWeight: 500,
                }}
                endIcon={ <KeyboardArrowDownIcon /> }
            >
                { getShortenedKey(walletState.wallet.publicKey) }
            </LoadingButton>

            <Menu
                id="fade-menu"
                MenuListProps={{
                    'aria-labelledby': 'fade-button',
                }}
                anchorEl={ anchorEl }
                open={ isOpen }
                onClose={ handleClose }
                TransitionComponent={ Fade }
            >
                <MenuItem
                    onClick={ copyToClipboard }
                >
                    <ContentCopy
                        fontSize='small'
                        sx={{ mr: 3 }}
                    />

                    Copy Key
                </MenuItem>

                <Divider sx={{ mx: 1 }} />

                <MenuItem
                    onClick={ handleDisconnect }
                >
                    <LogoutIcon
                        fontSize='small'
                        sx={{ mr: 3 }}
                    />
                    Disconnect
                </MenuItem>
            </Menu>
        </>
    );
};