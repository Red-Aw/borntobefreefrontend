import React from 'react';
import { Typography, Grid, Box } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import { useSelector } from 'react-redux';
import { releaseVestedAmounts } from '../utils/contract';

export const TableItem = (props) => {
    const { isItemAvailable, title, isValueAvailable, value, isReleaseButtonAvailable = false, ...others } = props;

    const unlock = 'Unlock';

    const walletState = useSelector((state) => state.wallet);

    const handleRelease = () => {
        releaseVestedAmounts();
    };

    return (
        <Grid
            item
            md={3}
            sm={6}
            xs={12}
        >
            <Box
                sx={{
                    p: 1,
                    textTransform: 'uppercase',
                    backgroundColor: isItemAvailable ? 'table.lightGrey' : 'table.darkGrey',
                    height: '100%',
                }}
            >
                <Typography
                    variant='h5'
                >
                    { title }
                </Typography>

                {isValueAvailable &&
                    <Typography
                        variant='h4'
                        sx={{
                            mt: 12,
                            textAlign: 'right',
                            verticalAlign: 'bottom'
                        }}
                    >
                        { value }
                    </Typography>
                }

                {isReleaseButtonAvailable &&
                    <Box display="flex" justifyContent="flex-end">
                        <LoadingButton
                            onClick={ handleRelease }
                            size='small'
                            loading={ walletState.isLoading }
                            sx={{
                                color: 'neutral.200',
                                backgroundColor: 'golden.primary',
                                '&:hover': {
                                    backgroundColor: 'primary.main',
                                },
                                mt: 2,
                                align: 'right',
                                verticalAlign: 'bottom'
                            }}
                        >
                            <Typography
                                variant='h6'
                            >
                                { unlock }
                            </Typography>
                        </LoadingButton>
                    </Box>
                }
            </Box>
        </Grid>
    );
};