import { useState, useEffect } from 'react';
import {
    Box,
    Stack,
    IconButton,
    Toolbar,
    Divider,
    List
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import { NavItem } from './NavItem';
import { useSelector } from 'react-redux';
import { ConnectWallet } from './Wallet/ConnectWallet';
import { WalletInfo } from './Wallet/WalletInfo';
import { DashboardNavbarRoot } from '../utils/styledComponents';

const items = [
    {
        id: 'main',
        href: '/',
        title: 'Home'
    },
    {
        id: 'whatIsBTBF',
        href: '/whatIsBTBF',
        title: 'What is BTBF'
    },
    {
        id: 'whitepaper',
        href: '/whitepaper',
        title: 'Whitepaper'
    },
    {
        id: 'videoPresentation',
        href: '/videoPresentation',
        title: 'Video Presentation'
    },
    {
        id: 'tutorials',
        href: '/tutorials',
        title: 'Tutorials'
    },
    {
        id: 'swap',
        href: '/swap',
        title: 'Swap'
    },
];

export default function DashboardNavbar() {
    // Redux
    const walletState = useSelector((state) => state.wallet);

    // Navigation
    const [navigationOpen, setNavigationOpen] = useState(false);
    const handleNavigation = () => {
        setNavigationOpen((navigationOpen) => !navigationOpen);
    };

    const [backgroundColor, setBackgroundColor] = useState('background.transparent');
    useEffect(() => {
        window.addEventListener('scroll', handleBackgroundColor);
        return () => window.removeEventListener('scroll', handleBackgroundColor);
    });

    const handleBackgroundColor = () => {
        if (window.scrollY > 50) {
            setBackgroundColor('background.dark')
        } else {
            setBackgroundColor('background.transparent')
        }
    };

    return (
        <Box>
            <DashboardNavbarRoot
                position='static'
                sx={{
                    backgroundColor: backgroundColor,
                }}
            >
                <Toolbar
                    sx={{
                        display: 'flex',
                        justifyContent: 'space-between'
                    }}
                >
                    <Stack
                        direction='row'
                        alignItems='center'
                    >
                        <IconButton
                            onClick={ handleNavigation }
                            sx={{
                                display: {
                                    sm: 'flex',
                                    md: 'none'
                                }
                            }}
                        >
                            <MenuIcon
                                sx={{
                                    color: 'neutral.200',
                                }}
                            />
                        </IconButton>

                        <Box
                            sx={{
                                display: {
                                    xs: 'none',
                                    sm: 'none',
                                    md: 'inline-block'
                                },
                            }}
                            component='img'
                            alt='Logo'
                            src='/static/images/logoBig.png'
                        />
                        <Box
                            sx={{
                                display: {
                                    xs: 'inline-block',
                                    sm: 'inline-block',
                                    md: 'none'
                                }
                            }}
                            component='img'
                            alt='Logo'
                            src='/static/images/logoSmall.png'
                            style={{
                                maxHeight: '70%',
                            }}
                        />
                    </Stack>

                    <Stack
                        direction='row'
                        sx={{
                            display: {
                                xs: 'none',
                                sm: 'none',
                                md: 'flex'
                            }
                        }}
                    >
                        {items.map((item) => (
                            <NavItem
                                key={item.id}
                                icon={item.icon}
                                href={item.href}
                                title={item.title}
                            />
                        ))}
                    </Stack>

                    <Stack
                        direction='row'
                        alignItems='center'
                    >
                        { walletState.isConnected ? <WalletInfo /> : <ConnectWallet /> }
                    </Stack>
                </Toolbar>

                {navigationOpen &&
                    <List
                        sx={{
                            display: { sm: 'box', md: 'none' },
                            backgroundColor: 'background.dark'
                        }}
                    >
                        <Divider
                            sx={{
                                borderColor: 'neutral.800',
                            }}
                        />

                        {items.map((item) => (
                            <NavItem
                                key={item.id}
                                icon={item.icon}
                                href={item.href}
                                title={item.title}
                            />
                        ))}
                    </List>
                }
            </DashboardNavbarRoot>
        </Box>
    );
};