import {
    Box,
    Grid,
    Typography
} from '@mui/material';
import { DashboardLayout } from '../components/DashboardLayout';
import { StyledContainer } from '../utils/styledComponents';

const tutorialsText = 'Tutorials';

const Page = () => {
    return (
        <Box
            component='main'
            sx={{
                flexGrow: 1,
            }}
        >
            <StyledContainer
                maxWidth='lg'
                sx={{
                    py: 8
                }}
            >
                <Grid
                    container
                    spacing={3}
                    sx={{
                        pb: 6,
                    }}
                >
                    <Grid
                        item
                        lg={12}
                        md={12}
                        sm={12}
                        xs={12}
                    >
                        <Typography
                            variant='h1'
                            sx={{
                                mb: 3
                            }}
                        >
                            { tutorialsText }
                        </Typography>
                    </Grid>
                </Grid>
            </StyledContainer>
        </Box>
    )
};

Page.getLayout = (page) => (
    <DashboardLayout>
        {page}
    </DashboardLayout>
);

export default Page;
