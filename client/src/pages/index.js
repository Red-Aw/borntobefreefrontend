import { useEffect, useState } from 'react';
import {
    Box,
    Grid,
    Typography,
    Stack,
} from '@mui/material';
import { DashboardLayout } from '../components/DashboardLayout';
import { TableItem } from '../components/TableItem';
import { StyledContainer } from '../utils/styledComponents';
import { countdown } from '../utils/countdown';
import { useSelector } from 'react-redux';

const HeadingTextList = {
    areYouReady: 'Are You \n Ready',
    bornToBeFree: 'Born to be \n Free?',
    startInvestingNowFrom: 'Start investing now from',
};

const BodyTextList = {
    btbfAllowsYouToProfit: 'BTBF allows you to profit from the trading of financial resources that is carried out using a decentralized autonomous organization (DAO) and a smart contract on a public blockchain.',
};

const TableList = {
    myBtbfTokens: 'My BTBF tokens',
    nextDividendPayoutIn: 'Next dividend payout in',
    btbfPrice: 'BTBF price',
    lockedTokens: 'My locked tokens in vesting',
    nextUnlock: 'Next unlock',
    totalWalletCount: 'Total wallet count',
    sellTaxPercentage: 'Sell tax percentage',
    buyTaxPercentage: 'Buy tax percentage',
    burnPercentage: 'Burn of tax percentage',
    circulatingSupply: 'Circulating supply',
    maxSupply: 'Max supply',
    totalSupply: 'Total supply',
    burntTokenCount: 'Burnt token count',
};

const Page = () => {
    // Redux
    const walletState = useSelector((state) => state.wallet);
    const contractState = useSelector((state) => state.contract);

    // Timer
    const [nextDividendPayout, setNextDividendPayout] = useState('All dividends sent');
    const [nextVestingUnlock, setVestingUnlock] = useState('Nothing to unlock');

    const updateDividendPayoutTimestamp = () => {
        setNextDividendPayout(countdown(contractState.nextDividendPayoutTimestamp));
    };
    const updateVestingTimestamp = () => {
        setVestingUnlock(countdown(walletState.wallet.nextUnlockTimestamp));
    };

    useEffect(() => {
        if (contractState.nextDividendPayoutTimestamp > 0) {
            const intervalDividend = setInterval(() => updateDividendPayoutTimestamp(), 1000);
            return () => clearInterval(intervalDividend);
        }
    }, [contractState]);

    useEffect(() => {
        if (walletState.wallet.nextUnlockTimestamp > 0) {
            const intervalRelease = setInterval(() => updateVestingTimestamp(), 1000);
            return () => clearInterval(intervalRelease);
        }
    }, [walletState]);

    return (
        <Box
            component='main'
            sx={{
                flexGrow: 1,
            }}
        >
            <StyledContainer
                maxWidth='lg'
                sx={{
                    py: 8
                }}
            >
                <Grid
                    container
                    spacing={3}
                    sx={{
                        pb: 6,
                    }}
                >
                    <Grid item md={1}></Grid>

                    <Grid
                        item
                        md={10}
                        sm={12}
                        xs={12}
                    >
                        <Typography
                            variant='h3'
                        >
                            { HeadingTextList.areYouReady }
                        </Typography>

                        <Typography
                            variant='h1'
                            sx={{
                                mb: 3
                            }}
                        >
                            { HeadingTextList.bornToBeFree }
                        </Typography>

                        <Typography
                            variant='body2'
                            sx={{
                                mb: 6
                            }}
                        >
                            { BodyTextList.btbfAllowsYouToProfit }
                        </Typography>
                    </Grid>

                    <Grid item md={1}></Grid>
                </Grid>

                <Grid
                    container
                    spacing={2}
                    sx={{
                        pb: 6,
                    }}
                >
                    <TableItem
                        isItemAvailable={ walletState.wallet.balance }
                        title={ TableList.myBtbfTokens }
                        isValueAvailable={ walletState.wallet.balance }
                        value={ walletState.wallet.balance + ' ' + walletState.wallet.symbol }
                    />

                    <TableItem
                        isItemAvailable={ contractState.nextDividendPayoutTimestamp }
                        title={ TableList.nextDividendPayoutIn }
                        isValueAvailable={ contractState.nextDividendPayoutTimestamp }
                        value={ nextDividendPayout }
                    />

                    <TableItem
                        isItemAvailable={ contractState.pancakeSwapPrice }
                        title={ TableList.btbfPrice }
                        isValueAvailable={ contractState.pancakeSwapPrice }
                        value={ '$' + contractState.pancakeSwapPrice }
                    />

                    <TableItem
                        isItemAvailable={ walletState.wallet.lockedAmount }
                        title={ TableList.lockedTokens }
                        isValueAvailable={ walletState.wallet.lockedAmount }
                        value={ walletState.wallet.lockedAmount + ' ' + walletState.wallet.symbol }
                    />

                    <TableItem
                        isItemAvailable={ walletState.wallet.nextUnlockTimestamp }
                        title={ TableList.nextUnlock }
                        isValueAvailable={ walletState.wallet.nextUnlockTimestamp >= 0 }
                        value={ nextVestingUnlock }
                        isReleaseButtonAvailable={ walletState.wallet.isAmountReadyToBeUnlocked }
                    />

                    <TableItem
                        isItemAvailable={ contractState.sellTaxPercentage }
                        title={ TableList.sellTaxPercentage }
                        isValueAvailable={ contractState.sellTaxPercentage }
                        value={ contractState.sellTaxPercentage + '%' }
                    />

                    <TableItem
                        isItemAvailable={ contractState.buyTaxPercentage }
                        title={ TableList.buyTaxPercentage }
                        isValueAvailable={ contractState.buyTaxPercentage }
                        value={ contractState.buyTaxPercentage + '%' }
                    />

                    <TableItem
                        isItemAvailable={ contractState.burnPercentage }
                        title={ TableList.burnPercentage }
                        isValueAvailable={ contractState.burnPercentage }
                        value={ contractState.burnPercentage + '%' }
                    />

                    <TableItem
                        isItemAvailable={ contractState.totalWalletCount >= 0 }
                        title={ TableList.totalWalletCount }
                        isValueAvailable={ contractState.totalWalletCount >= 0 }
                        value={ contractState.totalWalletCount }
                    />

                    <TableItem
                        isItemAvailable={ contractState.circulatingSupply }
                        title={ TableList.circulatingSupply }
                        isValueAvailable={ contractState.circulatingSupply }
                        value={ contractState.circulatingSupply + ' ' + walletState.wallet.symbol }
                    />

                    <TableItem
                        isItemAvailable={ contractState.maxSupply }
                        title={ TableList.maxSupply }
                        isValueAvailable={ contractState.maxSupply }
                        value={ contractState.maxSupply + ' ' + walletState.wallet.symbol }
                    />

                    <TableItem
                        isItemAvailable={ contractState.totalSupply }
                        title={ TableList.totalSupply }
                        isValueAvailable={ contractState.totalSupply }
                        value={ contractState.totalSupply + ' ' + walletState.wallet.symbol }
                    />

                    <TableItem
                        isItemAvailable={ contractState.burntTokenCount }
                        title={ TableList.burntTokenCount }
                        isValueAvailable={ contractState.burntTokenCount }
                        value={ contractState.burntTokenCount + ' ' + walletState.wallet.symbol }
                    />
                </Grid>

                <Grid
                    container
                    spacing={2}
                    sx={{
                        pb: 6,
                    }}
                >
                    <Grid
                        item
                        md={12}
                    >
                        <Typography
                            variant='h2'
                            sx={{
                                textAlign: 'center'
                            }}
                        >
                            { HeadingTextList.startInvestingNowFrom } <strong>$0,20</strong>
                        </Typography>

                        <Stack
                            direction='row'
                            justifyContent='space-evenly'
                        >
                            <Box
                                component='img'
                                alt='BTBF Logo'
                                src='/static/images/logoSmall.png'
                                sx={{
                                    py: 4,
                                }}
                            />
                        </Stack>
                    </Grid>
                </Grid>
            </StyledContainer>
        </Box>
    )
};

Page.getLayout = (page) => (
    <DashboardLayout>
        {page}
    </DashboardLayout>
);

export default Page;
