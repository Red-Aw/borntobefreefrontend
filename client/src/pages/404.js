import NextLink from 'next/link';
import {
    Box,
    Button,
    Typography
} from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { Background } from '../utils/styledComponents';

const notFoundText = '404 \n The page not found';
const goBackToHome = 'Go Back To Home';

const NotFound = () => (
    <Background
        style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
        }}
    >
        <Box
            sx={{
                flexDirection: 'column',
                alignItems: 'center',
                display: 'flex',
            }}
        >
            <Typography
                variant='h1'
                align='center'
                sx={{
                    color: 'neutral.200',
                    pb: 4,
                }}
            >
                { notFoundText }
            </Typography>

            <NextLink
                href='/'
                passHref
            >
                <Button
                    size='large'
                    startIcon={  (<ArrowBackIcon fontSize='small' />) }
                    sx={{
                        backgroundColor: 'primary.main',
                        color: 'neutral.200',
                        mb: 6,
                        '&:hover': {
                            backgroundColor: 'golden.primary',
                        },
                    }}
                    variant='contained'
                >
                    <Typography
                        variant='h6'
                        sx={{
                            textTransform: 'uppercase',
                        }}
                    >
                        { goBackToHome }
                    </Typography>
                </Button>
            </NextLink>
        </Box>
    </Background>
);

export default NotFound;
