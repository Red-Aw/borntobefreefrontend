import NextLink from 'next/link';
import {
    Box,
    Button,
    Grid,
    Typography,
    Avatar,
} from '@mui/material';
import { DashboardLayout } from '../components/DashboardLayout';
import { StyledContainer } from '../utils/styledComponents';

const HeadingTextList = {
    howDoesItWork: 'How Does \n It Work?',
};

const BodyTextList = {
    justInThreeYears: 'Just in 3 years time you would have gained 1750% profit if you would have sold at the price of $65000. Now is your chance again, cryptocurrency prices are available at bargain prices and we believe that in the next bull market prices will reach new record highs.',
    weBelieveThat: 'We believe that right now we are in the Anger or even Depression stage, which are the best stages to make investments based on the psychology of a market cycle, please see the picture below.',
    someIndividuals: 'Some individuals are fond of crypto as a high-risk, speculative investment with fantastic profits. Governments and consumers have been shifting away from cash, which could potentially eliminate privacy as all transactions are now performed using debit and credit cards. Crypto coins restore anonymity to exchanges. Unfortunately, criminals also find them appealing. Businesses are annoyed that transferring funds digitally from one location to another can take days and that banks subtract a fee from credit card payments. The immediate transfer of value between private individuals without any fees is very desirable.',
    asCryptoIsShifting: 'As crypto is shifting more toward projects with real utility and use-cases, volatility is likely to decrease as velocity increases. Don`t miss your chance to invest in BTBF!',
};

const ButtonTextList = {
    experienceFinancialFreedomToday: 'Experience financial freedom today!',
}

const StepsList = [
    'You bought a BTBF token and keep it in your MetaMask crypto wallet.',
    '1x per quarter, you will receive as dividends 50% of the profit obtained using the Smart contract.',
    'You receive profit in proportion to the number of coins you own.',
];

const TimelineList = [
    'The initial total amount of BTBF coins is 10\'000\'000',
    'Later, as the pool grows, the number of BTBF coins is reduced to 5\'000\'000',
    'As the number of members increases, the value of your assets increases!',
    'As the value of your assets increases, so do the dividends!',
];

const HeadingData = [
    'Problem',
    'Solution',
]

const RowData = [
    {
        data: 'Price volatility makes crypto holders doubtful and fearful when price is decreasing.'
    },
    {
        data: 'BTBF algorithmic trading uses the opportunity of each price drop and increase, resulting in an increase in the mutual fund, which increases the value of your asset and the value of dividends to be paid. Using a simple funding model:',
        list: [
            '5% commission when buying',
            '10% commission on sale goes to the funding fund.'
        ]
    },
    {
        data: 'Buying crypto and storing it in a wallet does not generate daily profits.'
    },
    {
        data: 'You earn dividends every quarter from your BTBF coin active earnings.'
    },
    {
        data: 'There is no opportunity to earn from the participants of the global common market unless you are active and fast enough.'
    },
    {
        data: 'Through BTBF coins, you profit from the participants of the global common market.'
    },
];

const Page = () => (
    <Box
        component='main'
        sx={{
            flexGrow: 1,
        }}
    >
        <StyledContainer
            maxWidth='lg'
            sx={{
                py: 8
            }}
        >
            <Grid
                container
                spacing={3}
                sx={{
                    pb: 6,
                }}
            >
                <Grid
                    item
                    lg={12}
                    md={12}
                    sm={12}
                    xs={12}
                >
                    <Typography
                        variant='h1'
                        sx={{
                            pb: 6,
                        }}
                    >
                        { HeadingTextList.howDoesItWork }
                    </Typography>

                    <ol
                        style={{
                            fontSize: '1.2rem',
                            fontWeight: 500,
                        }}
                    >
                        {StepsList.map((listItem, index) => (
                            <li
                                key={ index }
                            >
                                <Typography
                                    variant='body2'
                                    sx={{
                                        py: 2,
                                    }}
                                >
                                    { listItem }
                                </Typography>
                            </li>
                        ))}
                    </ol>
                </Grid>
            </Grid>

            <Grid
                container
                spacing={2}
                sx={{
                    pb: 6,
                }}
            >
                {TimelineList.map((timeline, index) => (
                    <Grid
                        key={ index }
                        item
                        md={3}
                        sm={6}
                        xs={12}
                    >
                        <Typography
                            variant='body2'
                            sx={{
                                p: 4,
                                textAlign: 'center',
                                backgroundColor: 'table.lightGrey',
                                height: '100%',
                            }}
                        >
                            { timeline }
                        </Typography>
                    </Grid>
                ))}
            </Grid>

            <Grid
                container
                justifyContent='center'
                columnSpacing={ 1 }
                rowSpacing={ 2 }
                sx={{
                    py: 8,
                }}
            >
                {HeadingData.map((heading, index) => (
                    <Grid
                        key={ index }
                        item
                        md={6}
                        sm={6}
                        xs={6}
                    >
                        <Typography
                            variant='body2'
                            sx={{
                                p: 4,
                                textTransform: 'uppercase',
                                textAlign: 'center',
                                backgroundColor: 'table.lightGrey',
                            }}
                        >
                            { heading }
                        </Typography>
                    </Grid>
                ))}

                {RowData.map((row, index) => (
                    <Grid
                        key={ index }
                        item
                        md={6}
                        sm={6}
                        xs={6}
                    >
                        <Typography
                            variant='body2'
                            sx={{
                                p: 4,
                                backgroundColor: 'table.darkGrey',
                                height: '100%',
                            }}
                        >
                            { row.data }
                        </Typography>
                    </Grid>
                ))};
            </Grid>

            <Grid
                container
                spacing={3}
            >
                <Grid
                    item
                    lg={12}
                    md={12}
                    sm={12}
                    xs={12}
                >
                    <NextLink
                        href='/' // TODO: fix URL
                        passHref
                    >
                        <Button
                            size='large'
                            sx={{
                                backgroundColor: 'primary.main',
                                color: 'neutral.200',
                                mb: 6,
                                '&:hover': {
                                    backgroundColor: 'golden.primary',
                                },
                            }}
                            variant='contained'
                            endIcon={
                                <Avatar
                                    src={'/static/images/iconStyled.png'}
                                    sx={{
                                        width: '60px',
                                        height: '60px',
                                    }}
                                />
                            }
                        >
                            <Typography
                                variant='h6'
                                sx={{
                                    textTransform: 'uppercase',
                                }}
                            >
                                { ButtonTextList.experienceFinancialFreedomToday }
                            </Typography>
                        </Button>
                    </NextLink>
                </Grid>
            </Grid>
        </StyledContainer>
    </Box>
);

Page.getLayout = (page) => (
    <DashboardLayout>
        {page}
    </DashboardLayout>
);

export default Page;
