import {
    Box,
    Typography,
    List,
    ListItem,
    Accordion,
    AccordionSummary,
    AccordionDetails,
} from '@mui/material';
import { DashboardLayout } from '../components/DashboardLayout';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { StyledContainer, StyledNextLink } from '../utils/styledComponents';

const faqTitle = 'Frequently \n asked questions';

const faqDataList = [
    {
        question: 'Is there a referral program?',
        answer: 'Yes, you can access it here -',
        link: 'http://borntobefree.io/',
    },
    {
        question: 'What is crypto currency?',
        answer: 'A crypto currency is a digital currency that, instead of relying on a centralized authority to maintain records, relies on cryptography to verify and record transactions. Only 4.2% of the world owns crypto. As the technology progresses, more sectors accept cryptocurrency as a legitimate financial system. We are at the Early Adopters stage in the crypto world, don’t miss your chance to be part of the future!',
    },
    {
        question: 'What is crowdfunding?',
        answer: 'Crowdfunding is an alternative form of funding that involves raising different amounts of money (both large and small) from a large number of people for a specific goal, project, business or idea. Money transfers are made through a crowdfunding platform in exchange for a crypto coin BTBF.',
    },
    {
        question: 'What and how to do?',
        answer: '',
        list: [
            'Go to borntobefree.io',
            'Familiarize yourself with the rules.',
            'Fill out the form and perform the necessary actions.',
            'You need a MetaMask crypto wallet.',
            'Add Binance Smart Chain.',
            'Add token address.',
            'Add BUSD token address.',
        ],
    },
    {
        question: 'Advantages, risks and benefits of trading',
        answer: 'There is a risk of losing investment that arises from variables, such as price volatility, changes in the market, and other market elements. Trading financial resources and currencies is one of the most popular types of trading because of its fantastic profit potential.',
    },
    {
        question: 'What is BTBF unlock program?',
        answer: 'BTBF coins purchased in the rounds 1; 2; 3 are unfrozen every current month until all 100% of purchased coins are received. Dividends are paid out in full, for the amount of purchased BTBF coins.',
    },
    {
        question: 'What additional benefits are there?',
        answer: '',
        list: [
            'Make a profit from any BTBF buy/sell transaction worldwide.',
            'Anyone can afford to own the BTBF coin and use its offered benefits!',
        ]
    },
];

const Page = () => {
    return (
        <Box
            component='main'
            sx={{
                flexGrow: 1,
            }}
        >
            <StyledContainer
                maxWidth='lg'
                sx={{
                    py: 8
                }}
            >
                <Typography
                    variant='h1'
                    sx={{
                        pb: 6,
                    }}
                >
                    { faqTitle }
                </Typography>

                {faqDataList.map((faqData, index) => (
                    <Accordion
                        key={ index }
                        sx={{
                            backgroundColor: 'background.transparent',
                            color: 'neutral.200',
                        }}
                    >
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                        >
                            <Typography
                                variant='body2'
                                sx={{
                                    my: 1
                                }}
                            >
                                <strong>
                                    { faqData.question }
                                </strong>
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            {faqData.answer && faqData.answer.length > 0 &&
                                <Typography
                                    variant='body2'
                                    sx={{
                                        mb: 2
                                    }}
                                >
                                    { faqData.answer } {faqData.link && faqData.link.length > 0 &&
                                        <StyledNextLink
                                            href={ faqData.link }
                                            passHref
                                            legacyBehavior
                                        >
                                            <a
                                                target='_blank'
                                                style={{
                                                    color: 'white',
                                                    textDecoration: 'underline'
                                                }}
                                            >
                                                { faqData.link }
                                            </a>
                                        </StyledNextLink>
                                    }
                                </Typography>
                            }

                            {faqData.list && faqData.list.length > 0 &&
                                <List
                                    sx={{
                                        pl: 4,
                                        py: 0,
                                        listStyleType: 'disc',
                                    }}
                                >
                                    {faqData.list.map((listItem, index) => (
                                        <ListItem
                                            key={ index }
                                            sx={{
                                                display: 'list-item',
                                                py: 0,
                                            }}
                                        >
                                            <Typography
                                                variant='body2'
                                                sx={{
                                                    mb: 2
                                                }}
                                            >
                                                { listItem }
                                            </Typography>
                                        </ListItem>
                                    ))}
                                </List>
                            }
                        </AccordionDetails>
                    </Accordion>
                ))}
            </StyledContainer>
        </Box>
    )
}

Page.getLayout = (page) => (
    <DashboardLayout>
        {page}
    </DashboardLayout>
);

export default Page;
