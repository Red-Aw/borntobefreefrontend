import {
    Box,
    Grid,
    Typography,
    Stack
} from '@mui/material';
import { DashboardLayout } from '../components/DashboardLayout';
import { StyledContainer } from '../utils/styledComponents';

const HeadingTextList = {
    whatIs: 'What is',
    bornToBeFree: 'Born to be \n Free?',
};

const BodyTextList = {
    btbfRaisesFunding: 'BTBF raises funding for algorithmic trading. BTBF`s solution is cost-effective, easy to understand and able to deliver the expectation of generating passive benefits to a wide and growing range of digital asset holders while mitigating risks. BTBF is based on the use of secure, cost-effective and user-friendly blockchain and cryptocurrency project technology, introducing a unique crowdfunding solution.',
    withASingleBtbfCoin: 'With a single BTBF coin, users can access a wide range of decentralized digital asset management services and at the same time keep their growing development resources in a highly incentivized system and earn passive income. Transaction accuracy and security is ensured.',
};

const Page = () => (
    <Box
        component='main'
        sx={{
            flexGrow: 1,
        }}
    >
        <StyledContainer
            maxWidth='lg'
            sx={{
                py: 8
            }}
        >
            <Grid
                container
                spacing={3}
            >
                <Grid
                    item
                    lg={12}
                    md={12}
                    sm={12}
                    xs={12}
                >
                    <Typography
                        variant='h3'
                    >
                        { HeadingTextList.whatIs }
                    </Typography>

                    <Typography
                        variant='h1'
                        sx={{
                            pb: 6,
                        }}
                    >
                        { HeadingTextList.bornToBeFree }
                    </Typography>

                    <Typography
                        variant='body2'
                        sx={{
                            mb: 3
                        }}
                    >
                        { BodyTextList.btbfRaisesFunding }
                    </Typography>

                    <Typography
                        variant='body2'
                        sx={{
                            mb: 3
                        }}
                    >
                        { BodyTextList.withASingleBtbfCoin }
                    </Typography>

                    <Stack
                        direction='row'
                        justifyContent='space-evenly'
                    >
                        <Box
                            component='img'
                            alt='BTBF Logo'
                            src='/static/images/logoSmall.png'
                            sx={{
                                py: 4,
                            }}
                        />
                    </Stack>
                </Grid>
            </Grid>
        </StyledContainer>
    </Box>
);

Page.getLayout = (page) => (
    <DashboardLayout>
        {page}
    </DashboardLayout>
);

export default Page;
