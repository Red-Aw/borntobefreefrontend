import Head from 'next/head';
import { CacheProvider } from '@emotion/react';
import { ThemeProvider } from '@mui/material/styles';
import { CssBaseline } from '@mui/material';
import { emotionCache } from '../utils/emotionCache';
import { darkTheme } from '../theme/theme';
import store from '../redux/store';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';
import { SnackbarUtilsConfigurator } from '../utils/snackbar';

const clientSideEmotionCache = emotionCache();

const App = (props) => {
    const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
    const getLayout = Component.getLayout ?? ((page) => page);

    const styles = {
        behavior: 'smooth'
    }

    return (
        <CacheProvider value={ emotionCache }>
            <Head>
                <title>
                    Born To Be Free Token
                </title>
                <meta
                    name='viewport'
                    content='width=device-width, initial-scale=1'
                />
            </Head>
            <ThemeProvider theme={ darkTheme }>
                <SnackbarProvider>
                    <SnackbarUtilsConfigurator />
                    <Provider store={ store }>
                        <CssBaseline />
                        { getLayout(<Component styles={ styles } { ...pageProps } />) }
                    </Provider>
                </SnackbarProvider>
            </ThemeProvider>
        </CacheProvider>
    )
}

export default App;

