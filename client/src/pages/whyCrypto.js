import NextLink from 'next/link';
import {
    Box,
    Button,
    Grid,
    Typography,
    Avatar,
} from '@mui/material';
import { DashboardLayout } from '../components/DashboardLayout';
import { StyledContainer } from '../utils/styledComponents';

const HeadingTextList = {
    whyInvestIn: 'Why \n invest in',
    cryptocurrencyNow: 'Cryptocurrency \n now',
};

const BodyTextList = {
    justInThreeYears: 'Just in 3 years time you would have gained 1750% profit if you would have sold at the price of $65000. Now is your chance again, cryptocurrency prices are available at bargain prices and we believe that in the next bull market prices will reach new record highs.',
    weBelieveThat: 'We believe that right now we are in the Anger or even Depression stage, which are the best stages to make investments based on the psychology of a market cycle, please see the picture below.',
    someIndividuals: 'Some individuals are fond of crypto as a high-risk, speculative investment with fantastic profits. Governments and consumers have been shifting away from cash, which could potentially eliminate privacy as all transactions are now performed using debit and credit cards. Crypto coins restore anonymity to exchanges. Unfortunately, criminals also find them appealing. Businesses are annoyed that transferring funds digitally from one location to another can take days and that banks subtract a fee from credit card payments. The immediate transfer of value between private individuals without any fees is very desirable.',
    asCryptoIsShifting: 'As crypto is shifting more toward projects with real utility and use-cases, volatility is likely to decrease as velocity increases. Don`t miss your chance to invest in BTBF!',
};

const ButtonTextList = {
    becomeAnInvestor: 'Become an investor!',
}

const Page = () => (
    <Box
        component='main'
        sx={{
            flexGrow: 1,
        }}
    >
        <StyledContainer
            maxWidth='lg'
            sx={{
                py: 8
            }}
        >
            <Grid
                container
                spacing={3}
            >
                <Grid
                    item
                    lg={12}
                    md={12}
                    sm={12}
                    xs={12}
                >
                    <Typography
                        variant='h3'
                    >
                        { HeadingTextList.whyInvestIn }
                    </Typography>

                    <Typography
                        variant='h1'
                        sx={{
                            pb: 6,
                        }}
                    >
                        { HeadingTextList.cryptocurrencyNow }
                    </Typography>

                    <Typography
                        variant='body2'
                        sx={{
                            mb: 3
                        }}
                    >
                        { BodyTextList.justInThreeYears }
                    </Typography>

                    <Box
                        component='img'
                        alt='Trading View'
                        src='/static/images/tradingView.png'
                        sx={{
                            mb: 3,
                            width: '100%',
                        }}
                    />

                    <Typography
                        variant='body2'
                        sx={{
                            mb: 3
                        }}
                    >
                        { BodyTextList.weBelieveThat }
                    </Typography>

                    <Box
                        component='img'
                        alt='Trading View'
                        src='/static/images/marketCycle.png'
                        sx={{
                            mb: 3,
                            width: '100%',
                        }}
                    />

                    <Typography
                        variant='body2'
                        sx={{
                            mb: 3
                        }}
                    >
                        { BodyTextList.someIndividuals }
                    </Typography>

                    <Typography
                        variant='body2'
                        sx={{
                            mb: 3
                        }}
                    >
                        { BodyTextList.asCryptoIsShifting }
                    </Typography>

                    <NextLink
                        href='/' // TODO: fix URL
                        passHref
                    >
                        <Button
                            size='large'
                            sx={{
                                backgroundColor: 'primary.main',
                                color: 'neutral.200',
                                mb: 6,
                                '&:hover': {
                                    backgroundColor: 'golden.primary',
                                },
                            }}
                            variant='contained'
                            endIcon={
                                <Avatar
                                    src={'/static/images/iconStyled.png'}
                                    sx={{
                                        width: '60px',
                                        height: '60px',
                                    }}
                                />
                            }
                        >
                            <Typography
                                variant='h6'
                                sx={{
                                    textTransform: 'uppercase',
                                }}
                            >
                                { ButtonTextList.becomeAnInvestor }
                            </Typography>
                        </Button>
                    </NextLink>
                </Grid>
            </Grid>
        </StyledContainer>
    </Box>
);

Page.getLayout = (page) => (
    <DashboardLayout>
        {page}
    </DashboardLayout>
);

export default Page;
