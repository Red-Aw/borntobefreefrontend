import { configureStore } from '@reduxjs/toolkit';
import walletReducer from './slices/walletSlice';
import contractReducer from './slices/contractSlice';
import blockchainReducer from './slices/blockchainSlice';

const store = configureStore({
    reducer: {
        wallet: walletReducer,
        contract: contractReducer,
        blockchain: blockchainReducer,
    }
});

export default store;