import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    isRequestToBlockchain: false,
    isDisconnect: false,
};

export const blockchainSlice = createSlice({
    name: 'Blockchain',
    initialState: initialState,
    reducers: {
        setIsRequestToBlockchain: (state, action) => {
            state.isRequestToBlockchain = action.payload;
        },
        setIsDisconnect: (state, action) => {
            state.isDisconnect = action.payload;
        },
    }
})

export const { setIsRequestToBlockchain, setIsDisconnect } = blockchainSlice.actions;

export default blockchainSlice.reducer;