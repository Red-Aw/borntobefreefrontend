import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    isLoading: false,
    isConnected: false,
    wallet: {
        publicKey: null,
        balance: null,
        symbol: 'BTBF',
        lockedAmount: null,
        isAmountReadyToBeUnlocked: null,
        nextUnlockTimestamp: null,
    },
};

export const walletSlice = createSlice({
    name: 'Wallet',
    initialState: initialState,
    reducers: {
        connectToWallet: (state, action) => {
            state.isLoading = false;
            state.isConnected = true;
            state.wallet.publicKey = action.payload.publicKey;
            state.wallet.balance = action.payload.balance;
            state.wallet.lockedAmount = action.payload.lockedAmount;
            state.wallet.isAmountReadyToBeUnlocked = action.payload.isAmountReadyToBeUnlocked;
            state.wallet.nextUnlockTimestamp = action.payload.nextUnlockTimestamp;
        },
        disconnectFromWallet: state => {
            state.isConnected = false;
            state.wallet.publicKey = null;
            state.wallet.balance = null;
            state.wallet.lockedAmount = null;
            state.wallet.isAmountReadyToBeUnlocked = null;
            state.wallet.nextUnlockTimestamp = null;
        },
        setIsWalletLoading: (state, action) => {
            state.isLoading = action.payload;
        },
    }
})

export const { connectToWallet, disconnectFromWallet, setIsWalletLoading } = walletSlice.actions;

export default walletSlice.reducer;