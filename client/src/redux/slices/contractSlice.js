import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    isLoading: false,
    isConnected: false,
    maxSupply: 10000000,
    totalSupply: null,
    burntTokenCount: null,
    circulatingSupply: null,
    sellTaxPercentage: null,
    buyTaxPercentage: null,
    burnPercentage: null,
    totalWalletCount: null,
    pancakeSwapPrice: null,
    nextDividendPayoutTimestamp: parseInt(process.env.NEXT_PUBLIC_NEXT_DIVIDEND_PAYOUT_TIMESTAMP),
};

export const contractSlice = createSlice({
    name: 'Contract',
    initialState: initialState,
    reducers: {
        connectToContract: (state, action) => {
            state.isLoading = false;
            state.isConnected = true;
            state.totalSupply = action.payload.totalSupply;
            state.burntTokenCount = action.payload.burntTokenCount;
            state.circulatingSupply = action.payload.circulatingSupply;
            state.sellTaxPercentage = action.payload.sellTaxPercentage;
            state.buyTaxPercentage = action.payload.buyTaxPercentage;
            state.burnPercentage = action.payload.burnPercentage;
            state.totalWalletCount = action.payload.totalWalletCount;
        },
        disconnectFromContract: state => {
            state.isConnected = false;
            state.totalSupply = null;
            state.burntTokenCount = null;
            state.circulatingSupply = null;
            state.sellTaxPercentage = null;
            state.buyTaxPercentage = null;
            state.burnPercentage = null;
            state.totalWalletCount = null;
        },
        setIsContractLoading: (state, action) => {
            state.isLoading = action.payload;
        },
        setPancakeSwapPrice: (state, action) => {
            state.pancakeSwapPrice = action.payload;
        },
    }
})

export const { connectToContract, disconnectFromContract, setIsContractLoading, setPancakeSwapPrice } = contractSlice.actions;

export default contractSlice.reducer;