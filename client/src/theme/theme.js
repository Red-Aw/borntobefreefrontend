import { createTheme } from '@mui/material';

export const darkTheme = createTheme({
    palette: {
        neutral: {
            100: '#F3F4F6',
            200: '#E5E7EB',
            300: '#D1D5DB',
            400: '#9CA3AF',
            500: '#6B7280',
            600: '#4B5563',
            700: '#374151',
            800: '#1F2937',
            900: '#111827'
        },
        action: {
            active: '#6B7280',
            focus: 'rgba(55, 65, 81, 0.12)',
            hover: 'rgba(55, 65, 81, 0.04)',
            selected: 'rgba(55, 65, 81, 0.08)',
            disabledBackground: 'rgba(55, 65, 81, 0.12)',
            disabled: 'rgba(55, 65, 81, 0.26)'
        },
        background: {
            default: '#F9FAFC',
            paper: '#FFFFFF',
            dark: '#1E2126',
            transparent: 'transparent',
            blueShadow: 'rgba(11, 31, 80, 0.12)',
        },
        divider: '#E6E8F0',
        primary: {
            main: '#0F193C',
            light: '#828DF8',
            dark: '#3832A0',
            contrastText: '#FFFFFF'
        },
        secondary: {
            main: '#10B981',
            light: '#3FC79A',
            dark: '#0B815A',
            contrastText: '#FFFFFF'
        },
        success: {
            main: '#14B8A6',
            light: '#43C6B7',
            dark: '#0E8074',
            contrastText: '#FFFFFF'
        },
        info: {
            main: '#2196F3',
            light: '#64B6F7',
            dark: '#0B79D0',
            contrastText: '#FFFFFF'
        },
        warning: {
            main: '#FFB020',
            light: '#FFBF4C',
            dark: '#B27B16',
            contrastText: '#FFFFFF'
        },
        error: {
            main: '#D14343',
            light: '#DA6868',
            dark: '#922E2E',
            contrastText: '#FFFFFF'
        },
        text: {
            primary: '#121828',
            secondary: '#65748B',
            disabled: 'rgba(55, 65, 81, 0.48)'
        },
        metamask: {
            active: '#F6851B',
            hover: '#CD6116',
        },
        golden: {
            primary: '#EDB52D',
        },
        table: {
            lightGrey: 'rgba(68, 73, 82, 0.6)',
            darkGrey: 'rgba(30, 34, 42, 0.6)',
        }
    },
    shape: {
        borderRadius: 8
    },
    shadows: [
        'none',
        '0px 1px 1px rgba(100, 116, 139, 0.06), 0px 1px 2px rgba(100, 116, 139, 0.1)',
        '0px 1px 2px rgba(100, 116, 139, 0.12)',
        '0px 1px 4px rgba(100, 116, 139, 0.12)',
        '0px 1px 5px rgba(100, 116, 139, 0.12)',
        '0px 1px 6px rgba(100, 116, 139, 0.12)',
        '0px 1px 7px rgba(100, 116, 139, 0.12)',
        '0px 1px 8px rgba(100, 116, 139, 0.12)',
        '0px 1px 10px rgba(100, 116, 139, 0.12)',
    ],
    typography: {
        a: {
            fontSize: '1rem',
            fontWeight: 500,
            lineHeight: 1.75
        },
        button: {
            fontWeight: 600
        },
        fontFamily: '"Encode Sans", Helvetica, Arial, sans-serif',
        body1: {
            fontSize: '1rem',
            fontWeight: 400,
            lineHeight: 1.5
        },
        body2: {
            fontSize: '1.2rem',
            fontWeight: 500,
            letterSpacing: 1,
        },
        body3: {
            fontSize: '1rem',
            fontWeight: 500,
            lineHeight: 1.5
        },
        subtitle1: {
            fontSize: '1rem',
            fontWeight: 500,
            lineHeight: 1.75
        },
        subtitle2: {
            fontSize: '0.875rem',
            fontWeight: 500,
            lineHeight: 1.57
        },
        overline: {
            fontSize: '0.75rem',
            fontWeight: 600,
            letterSpacing: '0.5px',
            lineHeight: 2.5,
            textTransform: 'uppercase'
        },
        caption: {
            fontSize: '0.75rem',
            fontWeight: 400,
            lineHeight: 1.66
        },
        h1: {
            fontWeight: 300,
            fontSize: '4.5rem',
            textTransform: 'uppercase',
            whiteSpace: 'pre-line',
            color: '#EDB52D',
            letterSpacing: 4,
        },
        h2: {
            fontWeight: 300,
            fontSize: '3rem',
            textTransform: 'uppercase',
            whiteSpace: 'pre-line',
            letterSpacing: 4,
        },
        h3: {
            fontWeight: 300,
            fontSize: '2.5rem',
            textTransform: 'uppercase',
            whiteSpace: 'pre-line',
            letterSpacing: 4
        },
        h4: {
            fontWeight: 400,
            fontSize: '1.8rem',
            textTransform: 'uppercase',
            whiteSpace: 'pre-line',
            letterSpacing: 4
        },
        h5: {
            fontWeight: 400,
            fontSize: '1.2rem',
            textTransform: 'uppercase',
            whiteSpace: 'pre-line',
            letterSpacing: 4
        },
        h6: {
            fontWeight: 600,
            fontSize: '1.125rem',
            lineHeight: 1.375
        }
    },
    components: {
        MuiButton: {
            defaultProps: {
                disableElevation: true
            },
            styleOverrides: {
                root: {
                    textTransform: 'none'
                },
                sizeSmall: {
                    padding: '6px 16px'
                },
                sizeMedium: {
                    padding: '8px 20px'
                },
                sizeLarge: {
                    padding: '11px 24px'
                },
                textSizeSmall: {
                    padding: '7px 12px'
                },
                textSizeMedium: {
                    padding: '9px 16px'
                },
                textSizeLarge: {
                    padding: '12px 16px'
                }
            }
          },
        MuiButtonBase: {
            defaultProps: {
                disableRipple: true
            }
        },
    }
});
