# Frontend For BornToBeFree.io

## Technologies used

[ReactJs](https://reactjs.org/docs/getting-started.html)

[NextJs](https://mui.com/material-ui/getting-started/installation/)

[MUI](https://nextjs.org/docs/getting-started)

---

## Setup

1. Install dependencies: `npm install`
2. Set up environment variable file: `cp .env.mainnet .env` and adjust necessary variables
    - development / local environment file named: `.env.dev`
    - testnet environment file named: `.env.testnet`
    - mainnet environment file named: `.env.mainnet`
3. Run necessary commands

---

## Commands

**Starts Next.js in development mode** `npm run dev`
Open [localhost:3000](http://localhost:3000) or [0.0.0.0:3000](http://0.0.0.0:3000) to view it in browser.

**Builds the application for production usage** `npm run build`
See more in [React.js deployment](https://facebook.github.io/create-react-app/docs/deployment) or in
[Next.js deployment](https://nextjs.org/docs/deployment)

**Starts Next.js production server** `npm run start`

**Export to static HTML** `npm run export`
Which can be run standalone without the need of a Node.js server. It is recommended to only use next export if you don't need any of the unsupported features requiring a server.

**Set up Next.js built-in ESLint configuration** `npm run lint`
