// SPDX-License-Identifier: MIT

pragma solidity ^0.8.19;

import './BornToBeFreeToken.sol';

contract BTBFTokenMigrator {
    bool private _isAutoMigrated;
    mapping(address => bool) private _balanceMigrations;

    BornToBeFreeToken public _currentToken;
    BornToBeFreeToken public _newToken;

    constructor(address currentToken, address newToken) {
        _currentToken = BornToBeFreeToken(currentToken);
        _newToken = BornToBeFreeToken(newToken);
        _isAutoMigrated = false;
    }

    function autoMigrate() external {
        require(_isAutoMigrated == false, 'auto migration already finished');

        _isAutoMigrated == true;
        address[] memory balanceAmounts = _currentToken.getBalanceAccounts();
        for (uint256 i = 0; i < balanceAmounts.length; i++) {
            address currentAccount = balanceAmounts[i];
            if (_balanceMigrations[currentAccount]) {
                continue;
            }

            _balanceMigrations[currentAccount] = true;
            if (_currentToken.getIsBalanceAccount(currentAccount)) {
                uint256 tokenAmount = _currentToken.balanceOf(currentAccount);
                _newToken.transfer(currentAccount, tokenAmount);
            }
        }
    }

    function manualMigrate() external {
        require(_balanceMigrations[msg.sender] == false, 'migration already finished');

        _balanceMigrations[msg.sender] = true;
        if (_currentToken.getIsBalanceAccount(msg.sender)) {
            uint256 tokenAmount = _currentToken.balanceOf(msg.sender);
            _newToken.transfer(msg.sender, tokenAmount);
        }
    }
}