// SPDX-License-Identifier: MIT

pragma solidity ^0.8.19;

import "./interface/IBEP20.sol";
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./interface/IPancakeFactory.sol";
import "./interface/IPancakeRouter.sol";
import "./interface/IPancakePair.sol";

contract BornToBeFreeToken is Context, IBEP20, Ownable {
    mapping (address => uint256) private _balances;
    mapping (address => bool) private _isBalanceAccounts;
    mapping (address => bool) private _isExcludedFromBalancesAccounts;
    address[] private _balanceAccounts;

    mapping (address => mapping (address => uint256)) private _allowances;

    mapping (address => bool) private _isTaxAddress;

    uint256 private _totalSupply;
    uint256 private _communitySupply;
    uint256 private _angelRoundSupply;
    uint256 private _preSaleOneSupply;
    uint256 private _preSaleTwoSupply;
    uint256 private _marketingSupply;
    uint256 private _teamSupply;
    uint256 private _liquiditySupply;
    uint256 private _busdSupply;

    uint8 private _decimals;
    string private _symbol;
    string private _name;

    address private _angelRoundWallet;
    address private _preSaleOneWallet;
    address private _preSaleTwoWallet;
    address private _feeWallet;
    address private _communityWallet;
    address private _marketingWallet;
    address private _teamWallet;

    uint256 private _burnStopAmount;
    uint256 private _totalBurnAmount;
    uint8 private _sellTaxPercentage;
    uint8 private _buyTaxPercentage;
    uint8 private _burnTaxPercentage;

    IPancakeRouter02 private immutable _pancakeRouterV2;
    IPancakeFactory private immutable _pancakeFactory;
    address private immutable _pancakeRouterAddress;
    address private immutable _pancakeFactoryAddress;
    address private immutable _busdAddress;
    address private immutable _pancakePairAddress;

    bool private _initialized;

    uint private _launchDatetime;

    enum LockType {
        ANGEL_ROUND,
        PRESALE_ONE,
        PRESALE_TWO,
        MARKETING,
        TEAM
    }

    struct TimeLockStruct {
        uint256 amount;
        uint releaseTimestamp;
    }

    // Structure: Percentage uint | LockTime datetime
    // Percentage is with 2 digits after dot. Example: 8.5% => 850; 10% => 1000; 12.25% => 1225
    uint[2][] private _marketingStructure;
    mapping (address => TimeLockStruct[]) private _marketingLockedAmounts;

    uint[2][] private _teamStructure;
    mapping (address => TimeLockStruct[]) private _teamLockedAmounts;

    uint[2][] private _angelRoundStructure;
    mapping (address => TimeLockStruct[]) private _angelRoundLockedAmounts;

    uint[2][] private _presaleOneStructure;
    mapping (address => TimeLockStruct[]) private _presaleOneLockedAmounts;

    uint[2][] private _presaleTwoStructure;
    mapping (address => TimeLockStruct[]) private _presaleTwoLockedAmounts;

    event Lock(address account, uint256 amount, uint256 releaseTime);
    event Release(address account, uint256 amount);

    error InvalidLockType (LockType lockType);

    constructor(
        address feeWallet,
        address communityWallet,
        address marketingWallet,
        address teamWallet,
        address angelRoundWallet,
        address preSaleOneWallet,
        address preSaleTwoWallet,
        address pancakeswapRouter,
        address pancakeswapFactory,
        address busdAddress
    ) {
        _name = 'Born To Be Free';
        _symbol = 'BTBF';

        _totalSupply = 10000000 * 10**18;
        _angelRoundSupply = 1000000 * 10**18;
        _preSaleOneSupply = 2500000 * 10**18;
        _preSaleTwoSupply = 2000000 * 10**18;
        _communitySupply = 1000000 * 10**18;
        _marketingSupply = 1500000 * 10**18;
        _teamSupply = 1500000 * 10**18;
        _liquiditySupply = 500000 * 10**18;

        require(
            _totalSupply == _angelRoundSupply + _preSaleOneSupply + _preSaleTwoSupply + _communitySupply + _marketingSupply + _teamSupply + _liquiditySupply,
            'Amounts are not equal to total supply'
        );

        _decimals = 18;
        _burnStopAmount = _totalSupply / 2;
        _totalBurnAmount = 0;
        _sellTaxPercentage = 6;
        _buyTaxPercentage = 4;
        _burnTaxPercentage = 20;

        _angelRoundWallet = angelRoundWallet;
        _preSaleOneWallet = preSaleOneWallet;
        _preSaleTwoWallet = preSaleTwoWallet;
        _feeWallet = feeWallet;
        _communityWallet = communityWallet;
        _marketingWallet = marketingWallet;
        _teamWallet = teamWallet;

        _pancakeRouterAddress = pancakeswapRouter;
        _pancakeFactoryAddress = pancakeswapFactory;
        _busdAddress = busdAddress;

        IPancakeRouter02 pancakeRouterV2 = IPancakeRouter02(_pancakeRouterAddress);
        IPancakeFactory pancakeFactory = IPancakeFactory(_pancakeFactoryAddress);
        _pancakePairAddress = pancakeFactory.createPair(address(this), _busdAddress);
        _pancakeRouterV2 = pancakeRouterV2;
        _pancakeFactory = pancakeFactory;

        _liquiditySupply = _totalSupply - _angelRoundSupply - _preSaleOneSupply - _preSaleTwoSupply - _communitySupply - _marketingSupply - _teamSupply;
        _busdSupply = 200000 * 10**18;

        _balances[angelRoundWallet] = _angelRoundSupply;
        emit Transfer(address(0), angelRoundWallet, _angelRoundSupply);

        _balances[preSaleOneWallet] = _preSaleOneSupply;
        emit Transfer(address(0), preSaleOneWallet, _preSaleOneSupply);

        _balances[preSaleTwoWallet] = _preSaleTwoSupply;
        emit Transfer(address(0), preSaleTwoWallet, _preSaleTwoSupply);

        _balances[_communityWallet] = _communitySupply;
        emit Transfer(address(0), _communityWallet, _communitySupply);

        _balances[_marketingWallet] = _marketingSupply;
        emit Transfer(address(0), _marketingWallet, _marketingSupply);

        _balances[_teamWallet] = _teamSupply;
        emit Transfer(address(0), _teamWallet, _teamSupply);

        _balances[msg.sender] = _liquiditySupply;
        emit Transfer(address(0), msg.sender, _liquiditySupply);

        _isExcludedFromBalancesAccounts[address(this)] = true;
        _isExcludedFromBalancesAccounts[_msgSender()] = true;
        _isExcludedFromBalancesAccounts[_angelRoundWallet] = true;
        _isExcludedFromBalancesAccounts[_preSaleOneWallet] = true;
        _isExcludedFromBalancesAccounts[_preSaleTwoWallet] = true;
        _isExcludedFromBalancesAccounts[_feeWallet] = true;
        _isExcludedFromBalancesAccounts[_communityWallet] = true;
        _isExcludedFromBalancesAccounts[_marketingWallet] = true;
        _isExcludedFromBalancesAccounts[_teamWallet] = true;
        _isExcludedFromBalancesAccounts[_pancakeRouterAddress] = true;
        _isExcludedFromBalancesAccounts[_pancakePairAddress] = true;
        _isExcludedFromBalancesAccounts[_pancakeFactoryAddress] = true;
        _isExcludedFromBalancesAccounts[_busdAddress] = true;

        _initialized = false;
    }

    function initialize() public onlyOwner {
        require(!_initialized, 'Contract has already been initialized');
        _initialized = true;

        _isTaxAddress[_pancakeRouterAddress] = true;
        _isTaxAddress[_pancakePairAddress] = true;
        _isTaxAddress[_pancakeFactoryAddress] = true;
        _isTaxAddress[_busdAddress] = true;

        _launchDatetime = block.timestamp;

        _marketingStructure = [
            [1000, _launchDatetime + 30 days],
            [1000, _launchDatetime + 60 days],
            [1000, _launchDatetime + 90 days],
            [1000, _launchDatetime + 120 days],
            [1000, _launchDatetime + 150 days],
            [1000, _launchDatetime + 180 days],
            [1000, _launchDatetime + 210 days],
            [1000, _launchDatetime + 240 days],
            [1000, _launchDatetime + 240 days]
        ];

        uint twelveMonthCliff = _launchDatetime + 365 days;
        _teamStructure = [
            [850, twelveMonthCliff + 30 days],
            [850, twelveMonthCliff + 60 days],
            [850, twelveMonthCliff + 90 days],
            [850, twelveMonthCliff + 120 days],
            [850, twelveMonthCliff + 150 days],
            [850, twelveMonthCliff + 180 days],
            [850, twelveMonthCliff + 210 days],
            [850, twelveMonthCliff + 240 days],
            [850, twelveMonthCliff + 270 days],
            [850, twelveMonthCliff + 300 days],
            [850, twelveMonthCliff + 330 days],
            [650, twelveMonthCliff + 360 days]
        ];

        uint threeMonthCliff = _launchDatetime + 90 days;
        _angelRoundStructure = [
            [450, threeMonthCliff + 30 days],
            [450, threeMonthCliff + 60 days],
            [450, threeMonthCliff + 90 days],
            [450, threeMonthCliff + 120 days],
            [450, threeMonthCliff + 150 days],
            [450, threeMonthCliff + 180 days],
            [450, threeMonthCliff + 210 days],
            [450, threeMonthCliff + 240 days],
            [450, threeMonthCliff + 270 days],
            [450, threeMonthCliff + 300 days],
            [450, threeMonthCliff + 330 days],
            [450, threeMonthCliff + 360 days],
            [450, threeMonthCliff + 390 days],
            [450, threeMonthCliff + 420 days],
            [450, threeMonthCliff + 450 days],
            [450, threeMonthCliff + 480 days],
            [450, threeMonthCliff + 510 days],
            [450, threeMonthCliff + 540 days],
            [450, threeMonthCliff + 570 days],
            [450, threeMonthCliff + 600 days],
            [500, threeMonthCliff + 630 days]
        ];

        _presaleOneStructure = [
            [650, threeMonthCliff + 30 days],
            [650, threeMonthCliff + 60 days],
            [650, threeMonthCliff + 90 days],
            [650, threeMonthCliff + 120 days],
            [650, threeMonthCliff + 150 days],
            [650, threeMonthCliff + 180 days],
            [650, threeMonthCliff + 210 days],
            [650, threeMonthCliff + 240 days],
            [650, threeMonthCliff + 270 days],
            [650, threeMonthCliff + 300 days],
            [650, threeMonthCliff + 330 days],
            [650, threeMonthCliff + 360 days],
            [650, threeMonthCliff + 390 days],
            [650, threeMonthCliff + 420 days],
            [400, threeMonthCliff + 450 days]
        ];

        _presaleTwoStructure = [
            [1000, threeMonthCliff + 30 days],
            [1000, threeMonthCliff + 60 days],
            [1000, threeMonthCliff + 90 days],
            [1000, threeMonthCliff + 120 days],
            [1000, threeMonthCliff + 150 days],
            [1000, threeMonthCliff + 180 days],
            [1000, threeMonthCliff + 210 days],
            [1000, threeMonthCliff + 240 days],
            [1000, threeMonthCliff + 240 days]
        ];

        _lockAmounts(_marketingWallet, _marketingSupply, LockType.MARKETING);
        _lockAmounts(_teamWallet, _teamSupply, LockType.TEAM);
    }

    /**
    * @dev Returns the bep token owner.
    */
    function getOwner() external view returns (address) {
        return owner();
    }

    /**
    * @dev Returns the token decimals.
    */
    function decimals() external view returns (uint8) {
        return _decimals;
    }

    /**
    * @dev Returns the token symbol.
    */
    function symbol() external view returns (string memory) {
        return _symbol;
    }

    /**
    * @dev Returns the token name.
    */
    function name() external view returns (string memory) {
        return _name;
    }

    /**
    * @dev See {BEP20-totalSupply}.
    */
    function totalSupply() external view returns (uint256) {
        return _totalSupply;
    }

    function totalBurnAmount() external view returns (uint256) {
        return _totalBurnAmount;
    }

    function sellTaxPercentage() external view returns (uint8) {
        return _sellTaxPercentage;
    }

    function buyTaxPercentage() external view returns (uint8) {
        return _buyTaxPercentage;
    }

    function burnPercentage() external view returns (uint8) {
        return _burnTaxPercentage;
    }

    function getBalanceAccounts() external view returns (address[] memory) {
        return _balanceAccounts;
    }

    function getIsBalanceAccount(address account) external view returns (bool) {
        return _isBalanceAccounts[account];
    }

    /**
    * @dev See {BEP20-balanceOf}.
    */
    function balanceOf(address account) external view returns (uint256) {
        return _balances[account];
    }

    function getLockedBalanceOf(address account) external view returns (uint256) {
        uint256 totalLockedBalancesAmount = 0;

        if (_angelRoundLockedAmounts[account].length > 0) {
            totalLockedBalancesAmount += _getLockedAmountsFromArray(_angelRoundLockedAmounts[account]);
        }

        if (_presaleOneLockedAmounts[account].length > 0) {
            totalLockedBalancesAmount += _getLockedAmountsFromArray(_presaleOneLockedAmounts[account]);
        }

        if (_presaleTwoLockedAmounts[account].length > 0) {
            totalLockedBalancesAmount += _getLockedAmountsFromArray(_presaleTwoLockedAmounts[account]);
        }

        if (_marketingLockedAmounts[account].length > 0) {
            totalLockedBalancesAmount += _getLockedAmountsFromArray(_marketingLockedAmounts[account]);
        }

        if (_teamLockedAmounts[account].length > 0) {
            totalLockedBalancesAmount += _getLockedAmountsFromArray(_teamLockedAmounts[account]);
        }

        return totalLockedBalancesAmount;
    }

    function getDetailedLockedBalanceOf(address account) external view returns (
        TimeLockStruct[] memory angelRoundLockedAmounts,
        TimeLockStruct[] memory presaleOneLockedAmounts,
        TimeLockStruct[] memory presaleTwoLockedAmounts,
        TimeLockStruct[] memory marketingLockedAmounts,
        TimeLockStruct[] memory teamLockedAmounts
    ) {
        return (
            _angelRoundLockedAmounts[account],
            _presaleOneLockedAmounts[account],
            _presaleTwoLockedAmounts[account],
            _marketingLockedAmounts[account],
            _teamLockedAmounts[account]
        );
    }

    /**
    * @dev See {BEP20-transfer}.
    *
    * Requirements:
    *
    * - `recipient` cannot be the zero address.
    * - the caller must have a balance of at least `amount`.
    */
    function transfer(address recipient, uint256 amount) external returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    /**
    * @dev See {BEP20-allowance}.
    */
    function allowance(address owner, address spender) external view returns (uint256) {
        return _allowances[owner][spender];
    }

    /**
    * @dev See {BEP20-approve}.
    *
    * Requirements:
    *
    * - `spender` cannot be the zero address.
    */
    function approve(address spender, uint256 amount) external returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    /**
    * @dev See {BEP20-transferFrom}.
    *
    * Emits an {Approval} event indicating the updated allowance. This is not
    * required by the EIP. See the note at the beginning of {BEP20};
    *
    * Requirements:
    * - `sender` and `recipient` cannot be the zero address.
    * - `sender` must have a balance of at least `amount`.
    * - the caller must have allowance for `sender`'s tokens of at least
    * `amount`.
    */
    function transferFrom(address sender, address recipient, uint256 amount) public override returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, _msgSender(), _allowances[sender][_msgSender()] - amount);
        return true;
    }

    /**
    * @dev Atomically increases the allowance granted to `spender` by the caller.
    *
    * This is an alternative to {approve} that can be used as a mitigation for
    * problems described in {BEP20-approve}.
    *
    * Emits an {Approval} event indicating the updated allowance.
    *
    * Requirements:
    *
    * - `spender` cannot be the zero address.
    */
    function increaseAllowance(address spender, uint256 addedValue) public returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender] + addedValue);
        return true;
    }

    /**
    * @dev Atomically decreases the allowance granted to `spender` by the caller.
    *
    * This is an alternative to {approve} that can be used as a mitigation for
    * problems described in {BEP20-approve}.
    *
    * Emits an {Approval} event indicating the updated allowance.
    *
    * Requirements:
    *
    * - `spender` cannot be the zero address.
    * - `spender` must have allowance for the caller of at least
    * `subtractedValue`.
    */
    function decreaseAllowance(address spender, uint256 subtractedValue) public returns (bool) {
        _approve(_msgSender(), spender, _allowances[_msgSender()][spender] - subtractedValue);
        return true;
    }

    function setSellTaxPercentage(uint8 newTaxPercentage) public onlyOwner returns (bool) {
        require(newTaxPercentage >= 0, 'tax should be more than 0 percent');
        require(newTaxPercentage < _sellTaxPercentage, 'tax should be less than existing sell tax percent');
        _sellTaxPercentage = newTaxPercentage;

        return true;
    }

    function setBuyTaxPercentage(uint8 newTaxPercentage) public onlyOwner returns (bool) {
        require(newTaxPercentage >= 0, 'tax should be more than 0 percent');
        require(newTaxPercentage < _buyTaxPercentage, 'tax should be less than existing buy tax percent');
        _buyTaxPercentage = newTaxPercentage;

        return true;
    }

    function releaseAmountsFor(address account) public {
        if (_angelRoundLockedAmounts[account].length > 0) {
            _releaseLockedAmountsFromArray(account, _angelRoundLockedAmounts[account]);
        }

        if (_presaleOneLockedAmounts[account].length > 0) {
            _releaseLockedAmountsFromArray(account, _presaleOneLockedAmounts[account]);
        }

        if (_presaleTwoLockedAmounts[account].length > 0) {
            _releaseLockedAmountsFromArray(account, _presaleTwoLockedAmounts[account]);
        }

        if (_marketingLockedAmounts[account].length > 0) {
            _releaseLockedAmountsFromArray(account, _marketingLockedAmounts[account]);
        }

        if (_teamLockedAmounts[account].length > 0) {
            _releaseLockedAmountsFromArray(account, _teamLockedAmounts[account]);
        }
    }

    /**
    * @dev Moves tokens `amount` from `sender` to `recipient`.
    *
    * This is internal function is equivalent to {transfer}, and can be used to
    * e.g. implement automatic token fees, slashing mechanisms, etc.
    *
    * Emits a {Transfer} event.
    *
    * Requirements:
    *
    * - `sender` cannot be the zero address.
    * - `recipient` cannot be the zero address.
    * - `sender` must have a balance of at least `amount`.
    */
    function _transfer(address sender, address recipient, uint256 amount) internal {
        require(sender != address(0), "BEP20: transfer from the zero address");
        require(recipient != address(0), "BEP20: transfer to the zero address");
        uint256 senderBalance = _balances[sender];
        require(senderBalance >= amount, 'transfer amount exceeds balance');

        if (!_initialized) {
            require(
                sender != _angelRoundWallet && sender != _preSaleOneWallet && sender != _preSaleTwoWallet,
                'must be initialized before transfer'
            );
        }

        if (!_isExcludedFromBalancesAccounts[recipient] && !_isBalanceAccounts[recipient]) {
            _isBalanceAccounts[recipient] = true;
            _balanceAccounts.push(recipient);
        }

        uint256 amountAfterTax = 0;
        if (_isTaxAddress[sender]) {
            amountAfterTax = amount - _tax(sender, amount, _buyTaxPercentage);
        } else if (_isTaxAddress[recipient]) {
            amountAfterTax = amount - _tax(sender, amount, _sellTaxPercentage);
        } else{
            amountAfterTax = amount;
        }

        _balances[sender] = senderBalance - amount;
        _balances[recipient] = _balances[recipient] + amountAfterTax;
        emit Transfer(sender, recipient, amountAfterTax);

        if (sender == _angelRoundWallet) {
            _lockAmounts(recipient, amountAfterTax, LockType.ANGEL_ROUND);
        } else if (sender == _preSaleOneWallet) {
            _lockAmounts(recipient, amountAfterTax, LockType.PRESALE_ONE);
        } else if (sender == _preSaleTwoWallet) {
            _lockAmounts(recipient, amountAfterTax, LockType.PRESALE_TWO);
        }
    }

    /**
    * @dev Sets `amount` as the allowance of `spender` over the `owner`s tokens.
    *
    * This is internal function is equivalent to `approve`, and can be used to
    * e.g. set automatic allowances for certain subsystems, etc.
    *
    * Emits an {Approval} event.
    *
    * Requirements:
    *
    * - `owner` cannot be the zero address.
    * - `spender` cannot be the zero address.
    */
    function _approve(address owner, address spender, uint256 amount) internal {
        require(owner != address(0), "BEP20: approve from the zero address");
        require(spender != address(0), "BEP20: approve to the zero address");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    function _tax(address sender, uint256 amount, uint8 taxPercentage) private returns (uint256) {
        if (taxPercentage == 0) {
            return 0;
        }

        uint256 totalTaxAmount = _calculateTaxAmount(amount, taxPercentage);
        uint256 taxAmountAferBurn = totalTaxAmount - _burn(sender, _calculateBurnAmount(totalTaxAmount));

        _balances[sender] = _balances[sender] - taxAmountAferBurn;
        _balances[_feeWallet] = _balances[_feeWallet] + taxAmountAferBurn;
        emit Transfer(sender, _feeWallet, taxAmountAferBurn);

        return totalTaxAmount;
    }

    function _calculateTaxAmount(uint256 amount, uint8 taxPercentage) private pure returns (uint256) {
        require(taxPercentage > 0, 'tax should be more than 0 percent');
        require(taxPercentage <= 100, 'tax should be less than 100 percent');

        return (amount * taxPercentage) / 100;
    }

    function _burn(address account, uint256 amount) private returns (uint256) {
        require(account != address(0), "BEP20: burn from the zero address");
        if (amount > 0) {
            _balances[account] = _balances[account] - amount;
            _totalSupply = _totalSupply - amount;
            _totalBurnAmount = _totalBurnAmount + amount;
            emit Transfer(account, address(0), amount);
        }

        return amount;
    }

    function _calculateBurnAmount(uint256 amount) private view returns (uint256) {
        uint256 burnAmount = 0;

        if (amount == 0) {
            return burnAmount;
        }

        if (_totalSupply > _burnStopAmount) {
            burnAmount = (amount * _burnTaxPercentage) / 100;
            uint256 availableBurn = _totalSupply - _burnStopAmount;

            if (burnAmount > availableBurn) {
                burnAmount = availableBurn;
            }
        }

        return burnAmount;
    }

    function _lockAmounts(
        address beneficiary,
        uint256 amount,
        LockType lockType
    ) private returns (bool) {
        require(amount > 0, 'amount to lock must be greater than 0');

        uint256 totalAmountToLock = 0;
        if (lockType == LockType.ANGEL_ROUND) {
            totalAmountToLock += _lockAmountsForStructure(
                _angelRoundStructure,
                _angelRoundLockedAmounts[beneficiary],
                beneficiary,
                amount
            );
        } else if (lockType == LockType.PRESALE_ONE) {
            totalAmountToLock += _lockAmountsForStructure(
                _presaleOneStructure,
                _presaleOneLockedAmounts[beneficiary],
                beneficiary,
                amount
            );
        } else if (lockType == LockType.PRESALE_TWO) {
            totalAmountToLock += _lockAmountsForStructure(
                _presaleTwoStructure,
                _presaleTwoLockedAmounts[beneficiary],
                beneficiary,
                amount
            );
        } else if (lockType == LockType.MARKETING) {
            totalAmountToLock += _lockAmountsForStructure(
                _marketingStructure,
                _marketingLockedAmounts[beneficiary],
                beneficiary,
                amount
            );
        } else if (lockType == LockType.TEAM) {
            totalAmountToLock += _lockAmountsForStructure(
                _teamStructure,
                _teamLockedAmounts[beneficiary],
                beneficiary,
                amount
            );
        } else {
            revert InvalidLockType({ lockType: lockType });
        }

        if (totalAmountToLock > 0) {
            _transfer(beneficiary, address(this), totalAmountToLock);
        }

        return true;
    }

    function _lockAmountsForStructure(
        uint[2][] storage structure,
        TimeLockStruct[] storage beneficiaryLockedAmountArray,
        address beneficiary,
        uint256 amount
    ) private returns (uint256) {
        uint256 totalAmountToLock = 0;

        for (uint i = 0; i < structure.length; i++) {
            if (structure[i][1] <= block.timestamp) {
                continue;
            }

            uint256 amountToLock = (amount * structure[i][0]) / 10000;
            totalAmountToLock += amountToLock;

            TimeLockStruct memory v;
            v.amount = amountToLock;
            v.releaseTimestamp = structure[i][1];

            beneficiaryLockedAmountArray.push(v);

            emit Lock(beneficiary, amountToLock, structure[i][1]);
        }

        return totalAmountToLock;
    }

    function _releaseLockedAmountsFromArray(address beneficiary, TimeLockStruct[] storage lockedStructureArray) private {
        uint256 totalAmountToRelease = 0;

        for (uint i = 0; i < lockedStructureArray.length; i++) {
            TimeLockStruct storage v = lockedStructureArray[i];

            if (v.releaseTimestamp > block.timestamp || v.amount == 0) {
                continue;
            }

            uint256 amountToRelease = v.amount;
            totalAmountToRelease += v.amount;
            v.amount = 0;
            emit Release(beneficiary, amountToRelease);
        }

        if (totalAmountToRelease > 0) {
            _transfer(address(this), beneficiary, totalAmountToRelease);
        }
    }

    function _getLockedAmountsFromArray(TimeLockStruct[] storage lockedStructureArray) private view returns (uint256) {
        uint256 totalLockedAmount = 0;

        for (uint i = 0; i < lockedStructureArray.length; i++) {
            TimeLockStruct storage v = lockedStructureArray[i];

            if (v.amount == 0) {
                continue;
            }

            if (v.releaseTimestamp > block.timestamp) {
                totalLockedAmount += v.amount;
            }
        }

        return totalLockedAmount;
    }
}