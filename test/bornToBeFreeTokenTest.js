const BornToBeFreeToken = artifacts.require('BornToBeFreeToken');
const { time } = require('@openzeppelin/test-helpers');

// Notice: Please comment the busd and pancake addresses in BornToBeFreeToken.sol before testing
// Token must be migrated before testing can launch
contract('BornToBeFreeToken Test', function (accounts) {
    const [owner, feeWallet, communityWallet, marketingWallet, teamWallet, angelRoundWallet, preSaleOneWallet,
        preSaleTwoWallet, pancake, randomWalletOne, randomWalletTwo, randomWalletThree, randomWalletFour, _] = accounts;

    const SELL = BigInt(6);
    const BUY = BigInt(4);

    function addDecimals(amount) {
        return amount === 0
            ? amount.toString()
            : amount.toString() + '000000000000000000';
    }

    function getVestedAmounts(list, now) {
        let lockedAmount = BigInt(0);
        let amountReadyToBeUnlocked = BigInt(0);

        list.forEach(lockedStructure => {
            if (lockedStructure.amount === '0') {
                return;
            }

            if (lockedStructure.releaseTimestamp > now) {
                lockedAmount += BigInt(lockedStructure.amount);
            } else {
                amountReadyToBeUnlocked += BigInt(lockedStructure.amount);
            }
        });

        return {
            'lockedAmount': lockedAmount,
            'amountReadyToBeUnlocked': amountReadyToBeUnlocked
        };
    }

    function getTaxAndBurnAmounts(amount, taxPercentage, totalBurnAmount) {
        if (taxPercentage === 0) {
            return {
                'taxAmount': 0,
                'burnAmount': 0
            };
        }

        let taxAmount = BigInt(amount) * BigInt(taxPercentage) / BigInt(100);

        if (BigInt(totalBurnAmount) >= BigInt(addDecimals(5000000))) {
            return {
                'taxAmount': taxAmount,
                'burnAmount': 0
            };
        }

        let burnAmount = BigInt(taxAmount) / BigInt(5);

        return {
            'taxAmount': BigInt(taxAmount) - BigInt(burnAmount),
            'burnAmount': burnAmount
        };
    }

    function getPercentageOfAmount(amount, percentage) {
        return BigInt(amount) * BigInt(percentage) / BigInt(100);
    }

    it('deployment', async function() {
        let contractInstance = await BornToBeFreeToken.deployed();

        let contractOwner = await contractInstance.getOwner();
        assert.equal(contractOwner, owner, 'contract owner should owner address');

        let contractTotalSupply = await contractInstance.totalSupply();
        assert.equal(contractTotalSupply.toString(), addDecimals(10000000), 'contract total supply should be 10000000');

        let contractBurnTotalAmount = await contractInstance.totalBurnAmount();
        assert.equal(contractBurnTotalAmount.toString(), addDecimals(0), 'contract total burn amount should be 0');

        let contractSellTaxPercentage = await contractInstance.sellTaxPercentage();
        assert.equal(contractSellTaxPercentage.toString(), '6', 'sell tax should be 6%');

        let contractBuyTaxPercentage = await contractInstance.buyTaxPercentage();
        assert.equal(contractBuyTaxPercentage.toString(), '4', 'buy tax should be 4%');

        let contractBurnPercentage = await contractInstance.burnPercentage();
        assert.equal(contractBurnPercentage.toString(), '20', 'burn should be 20%');

        let balanceOfOwner = await contractInstance.balanceOf(owner);
        assert.equal(balanceOfOwner.toString(), addDecimals(500000), 'owner balance should be 500000 tokens after deployment');

        let balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWallet.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens after deployment');

        let balanceOfCommunityWallet = await contractInstance.balanceOf(communityWallet);
        assert.equal(balanceOfCommunityWallet.toString(), addDecimals(1000000), 'community wallet balance should be 1000000 tokens after deployment');

        let balanceOfMarketingWallet = await contractInstance.balanceOf(marketingWallet);
        assert.equal(balanceOfMarketingWallet.toString(), addDecimals(1500000), 'marketing wallet balance should be 1500000 tokens after deployment');

        let balanceOfTeamWallet = await contractInstance.balanceOf(teamWallet);
        assert.equal(balanceOfTeamWallet.toString(), addDecimals(1500000), 'team wallet balance should be 1500000 tokens after deployment');

        let balanceOfAngelRoundWallet = await contractInstance.balanceOf(angelRoundWallet);
        assert.equal(balanceOfAngelRoundWallet.toString(), addDecimals(1000000), 'angel round wallet balance should be 1000000 tokens after deployment');

        let balanceOfPreSaleOneWallet = await contractInstance.balanceOf(preSaleOneWallet);
        assert.equal(balanceOfPreSaleOneWallet.toString(), addDecimals(2500000), 'preSale one wallet balance should be 2500000 tokens after deployment');

        let balanceOfPreSaleTwoWallet = await contractInstance.balanceOf(preSaleTwoWallet);
        assert.equal(balanceOfPreSaleTwoWallet.toString(), addDecimals(2000000), 'preSale two wallet balance should be 2000000 tokens after deployment');

        let balanceOfContractWallet = await contractInstance.balanceOf(contractInstance.address);
        assert.equal(balanceOfContractWallet.toString(), addDecimals(0), 'contract wallet balance should be 0 tokens after deployment');
    });

    it('before initialization', async function() {
        let contractInstance = await BornToBeFreeToken.deployed();

        let tokensToTransfer = addDecimals(100000);

        try {
            await contractInstance.transfer(randomWalletOne, web3.utils.toBN(tokensToTransfer), { from: angelRoundWallet });
            assert.fail('Transfer from angelRound wallet cannot happen before initialization is launched');
        } catch(err) {
            assert.include(err.message, 'revert', 'Error message contains `revert`');
        }

        try {
            await contractInstance.transfer(randomWalletTwo, web3.utils.toBN(tokensToTransfer), { from: preSaleOneWallet });
            assert.fail('Transfer from preSaleOne wallet cannot happen before initialization is launched');
        } catch(err) {
            assert.include(err.message, 'revert', 'Error message contains `revert`');
        }

        try {
            await contractInstance.transfer(randomWalletThree, web3.utils.toBN(tokensToTransfer), { from: preSaleTwoWallet });
            assert.fail('Transfer from preSaleTwo wallet cannot happen before initialization is launched');
        } catch(err) {
            assert.include(err.message, 'revert', 'Error message contains `revert`');
        }

        await contractInstance.transfer(randomWalletOne, web3.utils.toBN(tokensToTransfer), { from: owner });

        let balanceOfOwner = await contractInstance.balanceOf(owner);
        assert.equal(balanceOfOwner.toString(), addDecimals(400000), 'owner balance should be 400000 tokens after sending 100000 tokens');

        let balanceOfRandomWalletOne = await contractInstance.balanceOf(randomWalletOne);
        assert.equal(balanceOfRandomWalletOne.toString(), addDecimals(100000), 'random wallet one balance should be 100000 tokens receiving tokens');

        await contractInstance.transfer(owner, web3.utils.toBN(tokensToTransfer), { from: randomWalletOne });

        balanceOfOwner = await contractInstance.balanceOf(owner);
        assert.equal(balanceOfOwner.toString(), addDecimals(500000), 'owner balance should be 500000 tokens after receiving 100000 tokens');

        balanceOfRandomWalletOne = await contractInstance.balanceOf(randomWalletOne);
        assert.equal(balanceOfRandomWalletOne.toString(), addDecimals(0), 'random wallet one balance should be 0 tokens after sending 100000 tokens');
    });

    it('adding liquidity', async function () {
        let contractInstance = await BornToBeFreeToken.deployed();

        let tokensToTransfer = addDecimals(500000);
        await contractInstance.transfer(pancake, web3.utils.toBN(tokensToTransfer), { from: owner });

        let balanceOfOwner = await contractInstance.balanceOf(owner);
        assert.equal(balanceOfOwner.toString(), addDecimals(0), 'owner balance should be 0 tokens');

        // Improvisation of adding liquidity
        let balanceOfPancake = await contractInstance.balanceOf(pancake);
        assert.equal(balanceOfPancake.toString(), addDecimals(500000), 'pancake swap balance should be 0 tokens');
    });

    it('initialize', async function () {
        let contractInstance = await BornToBeFreeToken.deployed();

        try {
            await contractInstance.initialize({ from: feeWallet });
            assert.fail('Initialize can be launched only from owner wallet');
        } catch(err) {
            assert.include(err.message, 'revert', 'Error message contains `revert`');
        }

        await contractInstance.initialize();

        try {
            await contractInstance.initialize();
            assert.fail('Initialize cannot be launched again');
        } catch(err) {
            assert.include(err.message, 'revert', 'Error message contains `revert`');
        }

        let balanceOfMarketingWallet = await contractInstance.balanceOf(marketingWallet);
        assert.equal(balanceOfMarketingWallet.toString(), addDecimals(150000), 'marketing wallet balance should be 150000 tokens after initialize');

        let balanceOfTeamWallet = await contractInstance.balanceOf(teamWallet);
        assert.equal(balanceOfTeamWallet.toString(), addDecimals(0), 'team wallet balance should be 0 tokens after initialize');

        let balanceOfContractWallet = await contractInstance.balanceOf(contractInstance.address);
        assert.equal(balanceOfContractWallet.toString(), addDecimals(2850000), 'contract wallet balance should be 2850000 tokens after initialize');

        let contractBurnTotalAmount = await contractInstance.totalBurnAmount();
        assert.equal(contractBurnTotalAmount.toString(), addDecimals(0), 'contract total burn amount should be 0 after initialize');

        let isOwnerBalanceAccount = await contractInstance.getIsBalanceAccount(owner);
        assert.equal(isOwnerBalanceAccount, false, 'owner should not be balance account');

        let isFeeWalletBalanceAccount = await contractInstance.getIsBalanceAccount(feeWallet);
        assert.equal(isFeeWalletBalanceAccount, false, 'fee wallet should not be balance account');

        let isCommunityWalletBalanceAccount = await contractInstance.getIsBalanceAccount(communityWallet);
        assert.equal(isCommunityWalletBalanceAccount, false, 'community wallet should not be balance account');

        let isMarketingWalletBalanceAccount = await contractInstance.getIsBalanceAccount(marketingWallet);
        assert.equal(isMarketingWalletBalanceAccount, false, 'marketing wallet should not be balance account');

        let isTeamWalletBalanceAccount = await contractInstance.getIsBalanceAccount(teamWallet);
        assert.equal(isTeamWalletBalanceAccount, false, 'team wallet should not be balance account');

        let isAngelRoundWalletBalanceAccount = await contractInstance.getIsBalanceAccount(angelRoundWallet);
        assert.equal(isAngelRoundWalletBalanceAccount, false, 'angel round wallet should not be balance account');

        let isPreSaleOneWalletBalanceAccount = await contractInstance.getIsBalanceAccount(preSaleOneWallet);
        assert.equal(isPreSaleOneWalletBalanceAccount, false, 'preSale one wallet should not be balance account');

        let isPreSaleTwoWalletBalanceAccount = await contractInstance.getIsBalanceAccount(preSaleTwoWallet);
        assert.equal(isPreSaleTwoWalletBalanceAccount, false, 'preSale two wallet should not be balance account');

        let isContractAddressBalanceAccount = await contractInstance.getIsBalanceAccount(contractInstance.address);
        assert.equal(isContractAddressBalanceAccount, false, 'contract address should not be balance account');
    });

    it('vesting', async function() {
        let contractInstance = await BornToBeFreeToken.deployed();

        let balanceOfAngelRoundWalletBeforeTransactions = await contractInstance.balanceOf(angelRoundWallet);
        assert.equal(balanceOfAngelRoundWalletBeforeTransactions.toString(), addDecimals(1000000), 'angel round wallet balance should be 1000000 tokens before sending transactions');

        let balanceOfPreSaleOneWalletBeforeTransactions = await contractInstance.balanceOf(preSaleOneWallet);
        assert.equal(balanceOfPreSaleOneWalletBeforeTransactions.toString(), addDecimals(2500000), 'preSale one wallet balance should be 2500000 tokens before sending transactions');

        let balanceOfPreSaleTwoWalletBeforeTransactions = await contractInstance.balanceOf(preSaleTwoWallet);
        assert.equal(balanceOfPreSaleTwoWalletBeforeTransactions.toString(), addDecimals(2000000), 'preSale two wallet balance should be 2000000 tokens before sending transactions');

        let now = await time.latest();

        // Transaction
        let tokensToTransfer = addDecimals(500000);
        await contractInstance.transfer(randomWalletOne, web3.utils.toBN(tokensToTransfer), { from: angelRoundWallet });
        let balanceOfRandomWalletOneAfterTransaction = await contractInstance.balanceOf(randomWalletOne);
        assert.equal(balanceOfRandomWalletOneAfterTransaction.toString(), addDecimals(25000), 'random wallet one balance should be 25000 tokens');
        let isRandomWalletOneBalanceAccount = await contractInstance.getIsBalanceAccount(randomWalletOne);
        assert.equal(isRandomWalletOneBalanceAccount, true, 'random wallet one should be balance account');
        let randomWalletOneLockedBalances = await contractInstance.getLockedBalanceOf(randomWalletOne);
        let detailedRandomWalletOneLockedBalances = await contractInstance.getDetailedLockedBalanceOf(randomWalletOne);
        let vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalances[0], now);
        assert.equal(randomWalletOneLockedBalances.toString(), addDecimals(475000), 'random wallet one total locked amount should be 475000 tokens');
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(475000), 'random wallet one locked amount should be 475000 tokens for angel round');
        assert.equal(vestedAmounts.amountReadyToBeUnlocked.toString(), addDecimals(0), 'random wallet one ready to be unlocked amount should be 0 tokens');

        // Burn and fees
        let totalBurnAmount = await contractInstance.totalBurnAmount();
        assert.equal(totalBurnAmount.toString(), addDecimals(0), 'total burn amount should be 0 tokens');
        let balanceOfFeeWalletAfterTransaction = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWalletAfterTransaction.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');

        // Transaction
        tokensToTransfer = addDecimals(100000);
        await contractInstance.transfer(randomWalletTwo, web3.utils.toBN(tokensToTransfer), { from: angelRoundWallet });
        let balanceOfRandomWalletTwoAfterTransaction = await contractInstance.balanceOf(randomWalletTwo);
        assert.equal(balanceOfRandomWalletTwoAfterTransaction.toString(), addDecimals(5000), 'random wallet two balance should be 5000 tokens');
        let isRandomWalletTwoBalanceAccount = await contractInstance.getIsBalanceAccount(randomWalletTwo);
        assert.equal(isRandomWalletTwoBalanceAccount, true, 'random wallet two should be balance account');
        let randomWalletTwoLockedBalances = await contractInstance.getLockedBalanceOf(randomWalletTwo);
        let detailedRandomWalletTwoLockedBalances = await contractInstance.getDetailedLockedBalanceOf(randomWalletTwo);
        vestedAmounts = getVestedAmounts(detailedRandomWalletTwoLockedBalances[0], now);
        assert.equal(randomWalletTwoLockedBalances.toString(), addDecimals(95000), 'random wallet two total locked amount should be 95000 tokens');
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(95000), 'random wallet two locked amount should be 95000 tokens for angel round');
        assert.equal(vestedAmounts.amountReadyToBeUnlocked.toString(), addDecimals(0), 'random wallet two ready to be unlocked amount should be 0 tokens');

        // Burn and fees
        totalBurnAmount = await contractInstance.totalBurnAmount();
        assert.equal(totalBurnAmount.toString(), addDecimals(0), 'total burn amount should be 0 tokens');
        balanceOfFeeWalletAfterTransaction = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWalletAfterTransaction.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');

        // Transaction
        tokensToTransfer = addDecimals(2000000);
        await contractInstance.transfer(randomWalletThree, web3.utils.toBN(tokensToTransfer), { from: preSaleOneWallet });
        let balanceOfRandomWalletThreeAfterTransaction = await contractInstance.balanceOf(randomWalletThree);
        assert.equal(balanceOfRandomWalletThreeAfterTransaction.toString(), addDecimals(100000), 'random wallet three balance should be 100000 tokens');
        let isRandomWalletThreeBalanceAccount = await contractInstance.getIsBalanceAccount(randomWalletThree);
        assert.equal(isRandomWalletThreeBalanceAccount, true, 'random wallet three should be balance account');
        let randomWalletThreeLockedBalances = await contractInstance.getLockedBalanceOf(randomWalletThree);
        let detailedRandomWalletThreeLockedBalances = await contractInstance.getDetailedLockedBalanceOf(randomWalletThree);
        vestedAmounts = getVestedAmounts(detailedRandomWalletThreeLockedBalances[1], now);
        assert.equal(randomWalletThreeLockedBalances.toString(), addDecimals(1900000), 'random wallet three total locked amount should be 1900000 tokens');
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(1900000), 'random wallet three locked amount should be 1900000 tokens');
        assert.equal(vestedAmounts.amountReadyToBeUnlocked.toString(), addDecimals(0), 'random wallet three ready to be unlocked amount should be 0 tokens');

        // Burn and fees
        totalBurnAmount = await contractInstance.totalBurnAmount();
        assert.equal(totalBurnAmount.toString(), addDecimals(0), 'total burn amount should be 0 tokens');
        balanceOfFeeWalletAfterTransaction = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWalletAfterTransaction.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');

        // Transaction
        tokensToTransfer = addDecimals(500000);
        await contractInstance.transfer(randomWalletOne, web3.utils.toBN(tokensToTransfer), { from: preSaleOneWallet });
        balanceOfRandomWalletOneAfterTransaction = await contractInstance.balanceOf(randomWalletOne); // 25000 + 25000
        assert.equal(balanceOfRandomWalletOneAfterTransaction.toString(), addDecimals(50000), 'random wallet one balance should be 50000 tokens');
        randomWalletOneLockedBalances = await contractInstance.getLockedBalanceOf(randomWalletOne); // 475000 + 475000
        detailedRandomWalletOneLockedBalances = await contractInstance.getDetailedLockedBalanceOf(randomWalletOne);
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalances[1], now);
        assert.equal(randomWalletOneLockedBalances.toString(), addDecimals(950000), 'random wallet one total locked amount should be 950000 tokens');
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(475000), 'random wallet one locked amount should be 475000 tokens for preSale one');
        assert.equal(vestedAmounts.amountReadyToBeUnlocked.toString(), addDecimals(0), 'random wallet one ready to be unlocked amount should be 0 tokens');

        // Burn and fees
        totalBurnAmount = await contractInstance.totalBurnAmount();
        assert.equal(totalBurnAmount.toString(), addDecimals(0), 'total burn amount should be 0 tokens');
        balanceOfFeeWalletAfterTransaction = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWalletAfterTransaction.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');

        // Transaction
        tokensToTransfer = addDecimals(1800000);
        await contractInstance.transfer(randomWalletFour, web3.utils.toBN(tokensToTransfer), { from: preSaleTwoWallet });
        let balanceOfRandomWalletFourTransaction = await contractInstance.balanceOf(randomWalletFour);
        assert.equal(balanceOfRandomWalletFourTransaction.toString(), addDecimals(180000), 'random wallet four balance should be 180000 tokens');
        let isRandomWalletFourBalanceAccount = await contractInstance.getIsBalanceAccount(randomWalletFour);
        assert.equal(isRandomWalletFourBalanceAccount, true, 'random wallet four should be balance account');
        let randomWalletFourLockedBalances = await contractInstance.getLockedBalanceOf(randomWalletFour);
        let detailedRandomWalletFourLockedBalances = await contractInstance.getDetailedLockedBalanceOf(randomWalletFour);
        vestedAmounts = getVestedAmounts(detailedRandomWalletFourLockedBalances[2], now);
        assert.equal(randomWalletFourLockedBalances.toString(), addDecimals(1620000), 'random wallet four total locked amount should be 1620000 tokens');
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(1620000), 'random wallet four locked amount should be 1620000 tokens');
        assert.equal(vestedAmounts.amountReadyToBeUnlocked.toString(), addDecimals(0), 'random wallet four ready to be unlocked amount should be 0 tokens');

        // Burn and fees
        totalBurnAmount = await contractInstance.totalBurnAmount();
        assert.equal(totalBurnAmount.toString(), addDecimals(0), 'total burn amount should be 0 tokens');
        balanceOfFeeWalletAfterTransaction = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWalletAfterTransaction.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');

        // Transaction
        tokensToTransfer = addDecimals(200000);
        await contractInstance.transfer(randomWalletOne, web3.utils.toBN(tokensToTransfer), { from: preSaleTwoWallet });
        balanceOfRandomWalletOneAfterTransaction = await contractInstance.balanceOf(randomWalletOne); // 50000 + 20000
        assert.equal(balanceOfRandomWalletOneAfterTransaction.toString(), addDecimals(70000), 'random wallet one balance should be 70000 tokens');
        randomWalletOneLockedBalances = await contractInstance.getLockedBalanceOf(randomWalletOne); // 950000 + 180000
        detailedRandomWalletOneLockedBalances = await contractInstance.getDetailedLockedBalanceOf(randomWalletOne);
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalances[2], now);
        assert.equal(randomWalletOneLockedBalances.toString(), addDecimals(1130000), 'random wallet one total locked amount should be 1130000 tokens');
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(180000), 'random wallet one locked amount should be 180000 tokens for preSale two');
        assert.equal(vestedAmounts.amountReadyToBeUnlocked.toString(), addDecimals(0), 'random wallet one ready to be unlocked amount should be 0 tokens');

        // Burn and fees
        totalBurnAmount = await contractInstance.totalBurnAmount();
        assert.equal(totalBurnAmount.toString(), addDecimals(0), 'total burn amount should be 0 tokens');
        balanceOfFeeWalletAfterTransaction = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWalletAfterTransaction.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');

        // Check balances
        let balanceOfAngelRoundWalletAfterTransactions = await contractInstance.balanceOf(angelRoundWallet);
        assert.equal(balanceOfAngelRoundWalletAfterTransactions.toString(), addDecimals(400000), 'angel round wallet balance should be 400000 tokens after sending transactions');

        let balanceOfPreSaleOneWalletAfterTransactions = await contractInstance.balanceOf(preSaleOneWallet);
        assert.equal(balanceOfPreSaleOneWalletAfterTransactions.toString(), addDecimals(0), 'preSale one wallet balance should be 0 tokens after sending transactions');

        let balanceOfPreSaleTwoWalletAfterTransactions = await contractInstance.balanceOf(preSaleTwoWallet);
        assert.equal(balanceOfPreSaleTwoWalletAfterTransactions.toString(), addDecimals(0), 'preSale two wallet balance should be 0 tokens before sending transactions');

        await contractInstance.releaseAmountsFor(marketingWallet);
        await contractInstance.releaseAmountsFor(teamWallet);

        let balanceOfMarketingWallet = await contractInstance.balanceOf(marketingWallet);
        assert.equal(balanceOfMarketingWallet.toString(), addDecimals(150000), 'marketing wallet balance should be 150000 tokens');

        let balanceOfTeamWallet = await contractInstance.balanceOf(teamWallet);
        assert.equal(balanceOfTeamWallet.toString(), addDecimals(0), 'team wallet balance should be 0 tokens');

        // 1 months / 30 days
        await time.increase(time.duration.days(30));
        await time.advanceBlock();

        await contractInstance.releaseAmountsFor(marketingWallet);
        await contractInstance.releaseAmountsFor(teamWallet);
        await contractInstance.releaseAmountsFor(randomWalletOne);
        await contractInstance.releaseAmountsFor(randomWalletTwo);
        await contractInstance.releaseAmountsFor(randomWalletThree);
        await contractInstance.releaseAmountsFor(randomWalletFour);

        let balanceOfMarketingWalletAfterFirstMonthRelease = await contractInstance.balanceOf(marketingWallet);
        assert.equal(balanceOfMarketingWalletAfterFirstMonthRelease.toString(), addDecimals(300000), 'marketing wallet balance should be 300000 tokens after first month');

        let balanceOfTeamWalletAfterFirstMonthRelease = await contractInstance.balanceOf(teamWallet);
        assert.equal(balanceOfTeamWalletAfterFirstMonthRelease.toString(), addDecimals(0), 'team wallet balance should be 0 tokens after first month');

        let balanceOfRandomWalletOneAfterFirstMonth = await contractInstance.balanceOf(randomWalletOne);
        assert.equal(balanceOfRandomWalletOneAfterFirstMonth.toString(), addDecimals(70000), 'random wallet one balance should be 70000 tokens after first month');

        let balanceOfRandomWalletTwoAfterFirstMonth = await contractInstance.balanceOf(randomWalletTwo);
        assert.equal(balanceOfRandomWalletTwoAfterFirstMonth.toString(), addDecimals(5000), 'random wallet two balance should be 5000 tokens after first month');

        let balanceOfRandomWalletThreeAfterFirstMonth = await contractInstance.balanceOf(randomWalletThree);
        assert.equal(balanceOfRandomWalletThreeAfterFirstMonth.toString(), addDecimals(100000), 'random wallet three balance should be 100000 tokens after first month');

        let balanceOfRandomWalletFourAfterFirstMonth = await contractInstance.balanceOf(randomWalletFour);
        assert.equal(balanceOfRandomWalletFourAfterFirstMonth.toString(), addDecimals(180000), 'random wallet four balance should be 180000 tokens after first month');

        // 4 months / 120 days
        await time.increase(time.duration.days(30 * 3));
        await time.advanceBlock();
        now = await time.latest();

        await contractInstance.releaseAmountsFor(randomWalletOne);
        await contractInstance.releaseAmountsFor(randomWalletTwo);
        await contractInstance.releaseAmountsFor(randomWalletThree);
        await contractInstance.releaseAmountsFor(randomWalletFour);

        // Random wallet one
        // Angel round: from 475000 locked tokens 22500 tokens should be released (4.5%)
        // PreSale one: from 475000 locked tokens 32500 tokens should be released (6.5%)
        // PreSale two: from 180000 locked tokens 20000 tokens should be released (10%)
        let balanceOfRandomWalletOneAfterFourMonths = await contractInstance.balanceOf(randomWalletOne); // 70000 + 22500 + 32500 + 18000
        assert.equal(balanceOfRandomWalletOneAfterFourMonths.toString(), addDecimals(145000), 'random wallet one balance should be 145000 tokens after four months');
        let randomWalletOneLockedBalancesAfterFourMonths = await contractInstance.getLockedBalanceOf(randomWalletOne);
        let detailedRandomWalletOneLockedBalancesAfterFourMonths = await contractInstance.getDetailedLockedBalanceOf(randomWalletOne);
        assert.equal(randomWalletOneLockedBalancesAfterFourMonths.toString(), addDecimals(1055000), 'random wallet one total locked amount should be 1130000 tokens');
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalancesAfterFourMonths[0], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(452500), 'random wallet one locked amount should be 452500 tokens after four months for angel round');
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalancesAfterFourMonths[1], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(442500), 'random wallet one locked amount should be 442500 tokens after four months for preSale one');
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalancesAfterFourMonths[2], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(160000), 'random wallet one locked amount should be 160000 tokens after four months for preSale two');

        // Random wallet two
        // Angel round: from 95000 locked tokens 4500 tokens should be released (4.5%)
        let balanceOfRandomWalletTwoAfterFourMonths = await contractInstance.balanceOf(randomWalletTwo); // 5000 + 4500
        assert.equal(balanceOfRandomWalletTwoAfterFourMonths.toString(), addDecimals(9500), 'random wallet two balance should be 9500 tokens after four months');
        let randomWalletTwoLockedBalancesAfterFourMonths = await contractInstance.getLockedBalanceOf(randomWalletTwo);
        let detailedRandomWalletTwoLockedBalancesAfterFourMonths = await contractInstance.getDetailedLockedBalanceOf(randomWalletTwo);
        assert.equal(randomWalletTwoLockedBalancesAfterFourMonths.toString(), addDecimals(90500), 'random wallet two total locked amount should be 90500 tokens');
        vestedAmounts = getVestedAmounts(detailedRandomWalletTwoLockedBalancesAfterFourMonths[0], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(90500), 'random wallet two locked amount should be 86880 tokens after four months');

        // Random wallet three
        // PreSale one: from 1900000 locked tokens 130000 tokens should be released (6.5%)
        let balanceOfRandomWalletThreeAfterFourMonths = await contractInstance.balanceOf(randomWalletThree); // 100000 + 130000
        assert.equal(balanceOfRandomWalletThreeAfterFourMonths.toString(), addDecimals(230000), 'random wallet three balance should be 230000 tokens after four months');
        let randomWalletThreeLockedBalancesAfterFourMonths = await contractInstance.getLockedBalanceOf(randomWalletThree);
        let detailedRandomWalletThreeLockedBalancesAfterFourMonths = await contractInstance.getDetailedLockedBalanceOf(randomWalletThree);
        assert.equal(randomWalletThreeLockedBalancesAfterFourMonths.toString(), addDecimals(1770000), 'random wallet three total locked amount should be 1770000 tokens');
        vestedAmounts = getVestedAmounts(detailedRandomWalletThreeLockedBalancesAfterFourMonths[1], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(1770000), 'random wallet three locked amount should be 1770000 tokens after four months');

        // Random wallet four
        // PreSale two: from 1620000 locked tokens 180000 tokens should be released (10%)
        let balanceOfRandomWalletFourAfterFourMonths = await contractInstance.balanceOf(randomWalletFour); // 180000 + 180000
        assert.equal(balanceOfRandomWalletFourAfterFourMonths.toString(), addDecimals(360000), 'random wallet four balance should be 360000 tokens after four months');
        let randomWalletFourLockedBalancesAfterFourMonths = await contractInstance.getLockedBalanceOf(randomWalletFour);
        let detailedRandomWalletFourLockedBalancesAfterFourMonths = await contractInstance.getDetailedLockedBalanceOf(randomWalletFour);
        assert.equal(randomWalletFourLockedBalancesAfterFourMonths.toString(), addDecimals(1440000), 'random wallet four total locked amount should be 1440000 tokens');
        vestedAmounts = getVestedAmounts(detailedRandomWalletFourLockedBalancesAfterFourMonths[2], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(1440000), 'random wallet four locked amount should be 1440000 tokens after four months');

        // Transaction
        tokensToTransfer = addDecimals(400000);
        await contractInstance.transfer(randomWalletOne, web3.utils.toBN(tokensToTransfer), { from: angelRoundWallet });
        balanceOfRandomWalletOneAfterTransaction = await contractInstance.balanceOf(randomWalletOne); // 145000 + 38000
        assert.equal(balanceOfRandomWalletOneAfterTransaction.toString(), addDecimals(183000), 'random wallet one balance should be 183000 tokens');
        randomWalletOneLockedBalances = await contractInstance.getLockedBalanceOf(randomWalletOne);
        detailedRandomWalletOneLockedBalances = await contractInstance.getDetailedLockedBalanceOf(randomWalletOne);
        assert.equal(randomWalletOneLockedBalances.toString(), addDecimals(1417000), 'random wallet one total locked amount should be 1417000 tokens');
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalances[0], now); // 452500 + 362000
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(814500), 'random wallet one locked amount should be 814500 tokens for angel round');
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalances[1], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(442500), 'random wallet one locked amount should be 442500 tokens for preSale one');
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalances[2], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(160000), 'random wallet one locked amount should be 160000 tokens for preSale two');

        balanceOfAngelRoundWalletAfterTransactions = await contractInstance.balanceOf(angelRoundWallet);
        assert.equal(balanceOfAngelRoundWalletAfterTransactions.toString(), addDecimals(0), 'angel round wallet balance should be 0 tokens after sending transactions');

        // 9 months / 270 days
        await time.increase(time.duration.days(30 * 5));
        await time.advanceBlock();

        await contractInstance.releaseAmountsFor(marketingWallet);
        await contractInstance.releaseAmountsFor(teamWallet);

        let balanceOfMarketingWalletAfterNineMonthRelease = await contractInstance.balanceOf(marketingWallet);
        assert.equal(balanceOfMarketingWalletAfterNineMonthRelease.toString(), addDecimals(1500000), 'marketing wallet balance should be 1500000 tokens after nine month release');

        let balanceOfTeamWalletAfterNineMonthRelease = await contractInstance.balanceOf(teamWallet);
        assert.equal(balanceOfTeamWalletAfterNineMonthRelease.toString(), addDecimals(0), 'team wallet balance should be 0 tokens after nine month release');

        // 12 months / 365 days
        await time.increase(time.duration.days(95));
        await time.advanceBlock();

        await contractInstance.releaseAmountsFor(teamWallet);

        let balanceOfTeamWalletAfterTwelveMonthRelease = await contractInstance.balanceOf(teamWallet);
        assert.equal(balanceOfTeamWalletAfterTwelveMonthRelease.toString(), addDecimals(0), 'team wallet balance should be 0 tokens after twelve month release');

        // 13 months / 395 days
        await time.increase(time.duration.days(30));
        await time.advanceBlock();
        now = await time.latest();

        await contractInstance.releaseAmountsFor(teamWallet);
        await contractInstance.releaseAmountsFor(randomWalletOne);
        await contractInstance.releaseAmountsFor(randomWalletFour);

        let balanceOfTeamWalletAfterThirteenMonths = await contractInstance.balanceOf(teamWallet);
        assert.equal(balanceOfTeamWalletAfterThirteenMonths.toString(), addDecimals(127500), 'team wallet balance should be 127500 tokens after thirteen months');

        // Random wallet one
        // Angel round: from 814500 locked tokens 450000 are still locked
        // PreSale one: from 442500 locked tokens 150000 are still locked
        // PreSale two: fully unlocked remaining 160000 tokens
        let balanceOfRandomWalletOneAfterThirteenMonths = await contractInstance.balanceOf(randomWalletOne); // 183000 + 364500 + 292500 + 160000
        assert.equal(balanceOfRandomWalletOneAfterThirteenMonths.toString(), addDecimals(1000000), 'random wallet one balance should be 1000000 tokens after thirteen months');
        let randomWalletOneLockedBalancesAfterThirteenMonths = await contractInstance.getLockedBalanceOf(randomWalletOne);
        let detailedRandomWalletOneLockedBalancesAfterThirteenMonths = await contractInstance.getDetailedLockedBalanceOf(randomWalletOne);
        assert.equal(randomWalletOneLockedBalancesAfterThirteenMonths.toString(), addDecimals(600000), 'random wallet one total locked amount should be 600000 tokens');
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalancesAfterThirteenMonths[0], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(450000), 'random wallet one locked amount should be 450000 tokens after thirteen months for angel round');
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalancesAfterThirteenMonths[1], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(150000), 'random wallet one locked amount should be 150000 tokens after thirteen months for preSale one');
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalancesAfterThirteenMonths[2], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(0), 'random wallet one locked amount should be 0 tokens after thirteen months for preSale two');

        // Random wallet four
        // PreSale two: fully unlocked remaining 1440000 tokens
        let balanceOfRandomWalletFourAfterThirteenMonths = await contractInstance.balanceOf(randomWalletFour); // 360000 + 1440000
        assert.equal(balanceOfRandomWalletFourAfterThirteenMonths.toString(), addDecimals(1800000), 'random wallet four balance should be 1800000 tokens after thirteen months');
        let randomWalletFourLockedBalancesAfterThirteenMonths= await contractInstance.getLockedBalanceOf(randomWalletFour);
        let detailedRandomWalletFourLockedBalancesAfterThirteenMonths = await contractInstance.getDetailedLockedBalanceOf(randomWalletFour);
        assert.equal(randomWalletFourLockedBalancesAfterThirteenMonths.toString(), addDecimals(0), 'random wallet four total locked amount should be 0 tokens');
        vestedAmounts = getVestedAmounts(detailedRandomWalletFourLockedBalancesAfterThirteenMonths[2], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(0), 'random wallet four locked amount should be 0 tokens after thirteen months');


        // 19 months / 575 days
        await time.increase(time.duration.days(30 * 6));
        await time.advanceBlock();
        now = await time.latest();

        await contractInstance.releaseAmountsFor(randomWalletOne);
        await contractInstance.releaseAmountsFor(randomWalletThree);

        // Random wallet one
        // Angel round: from 450000 locked tokens 207000 are still locked
        // PreSale one: fully unlocked remaining 150000 tokens
        let balanceOfRandomWalletOneAfterNineteenMonths = await contractInstance.balanceOf(randomWalletOne); // 1000000 + 243000 + 150000
        assert.equal(balanceOfRandomWalletOneAfterNineteenMonths.toString(), addDecimals(1393000), 'random wallet one balance should be 1393000 tokens after nineteen months');
        let randomWalletOneLockedBalancesAfterNineteenMonths = await contractInstance.getLockedBalanceOf(randomWalletOne);
        let detailedRandomWalletOneLockedBalancesAfterNineteenMonths = await contractInstance.getDetailedLockedBalanceOf(randomWalletOne);
        assert.equal(randomWalletOneLockedBalancesAfterNineteenMonths.toString(), addDecimals(207000), 'random wallet one total locked amount should be 207000 tokens');
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalancesAfterNineteenMonths[0], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(207000), 'random wallet one locked amount should be 207000 tokens after nineteen for angel round');
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalancesAfterNineteenMonths[1], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(0), 'random wallet one locked amount should be 0 tokens after nineteen months for preSale one');

        // Random wallet three
        // PreSale one: fully unlocked remaining 1770000 tokens
        let balanceOfRandomWalletThreeAfterNineteenMonths = await contractInstance.balanceOf(randomWalletThree); // 230000 + 1770000
        assert.equal(balanceOfRandomWalletThreeAfterNineteenMonths.toString(), addDecimals(2000000), 'random wallet three balance should be 2000000 tokens after nineteen months');
        let randomWalletThreeLockedBalancesAfterNineteenMonths = await contractInstance.getLockedBalanceOf(randomWalletThree);
        let detailedRandomWalletThreeLockedBalancesAfterNineteenMonths = await contractInstance.getDetailedLockedBalanceOf(randomWalletThree);
        assert.equal(randomWalletThreeLockedBalancesAfterNineteenMonths.toString(), addDecimals(0), 'random wallet three total locked amount should be 0 tokens');
        vestedAmounts = getVestedAmounts(detailedRandomWalletThreeLockedBalancesAfterNineteenMonths[1], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(0), 'random wallet three locked amount should be 0 tokens after nineteen months');

        // 23 months / 695 days
        await time.increase(time.duration.days(30 * 4));
        await time.advanceBlock();

        await contractInstance.releaseAmountsFor(teamWallet);

        // Team wallet
        let balanceOfTeamWalletAfterTwentyThreeMonths = await contractInstance.balanceOf(teamWallet);
        assert.equal(balanceOfTeamWalletAfterTwentyThreeMonths.toString(), addDecimals(1402500), 'team wallet balance should be 1402500 tokens after twenty three months');

        // 24 months / 725 days
        await time.increase(time.duration.days(30));
        await time.advanceBlock();
        now = await time.latest();

        await contractInstance.releaseAmountsFor(teamWallet);
        await contractInstance.releaseAmountsFor(randomWalletOne);
        await contractInstance.releaseAmountsFor(randomWalletTwo);

        // Team wallet
        let balanceOfTeamWalletAfterTwentyFourMonths = await contractInstance.balanceOf(teamWallet);
        assert.equal(balanceOfTeamWalletAfterTwentyFourMonths.toString(), addDecimals(1500000), 'team wallet balance should be 1500000 tokens after twenty four months');

        // Random wallet one
        // Angel round: fully unlocked remaining 207000 tokens
        let balanceOfRandomWalletOneAfterTwentyFourMonths = await contractInstance.balanceOf(randomWalletOne); // 1393000 + 207000
        assert.equal(balanceOfRandomWalletOneAfterTwentyFourMonths.toString(), addDecimals(1600000), 'random wallet one balance should be 1600000 tokens after twenty four months');
        let randomWalletOneLockedBalancesAfterTwentyFourMonths = await contractInstance.getLockedBalanceOf(randomWalletOne);
        let detailedRandomWalletOneLockedBalancesAfterTwentyFourMonths = await contractInstance.getDetailedLockedBalanceOf(randomWalletOne);
        assert.equal(randomWalletOneLockedBalancesAfterTwentyFourMonths.toString(), addDecimals(0), 'random wallet one total locked amount should be 0 tokens');
        vestedAmounts = getVestedAmounts(detailedRandomWalletOneLockedBalancesAfterTwentyFourMonths[0], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(0), 'random wallet one locked amount should be 0 tokens after twenty four months');
        assert.equal(vestedAmounts.amountReadyToBeUnlocked.toString(), addDecimals(0), 'random wallet one ready to be unlocked amount should be 0 tokens after twenty four months');

        // Random wallet two
        // Angel round: fully unlocked remaining 90500 tokens
        let balanceOfRandomWalletTwoAfterTwentyFourMonths = await contractInstance.balanceOf(randomWalletTwo);
        assert.equal(balanceOfRandomWalletTwoAfterTwentyFourMonths.toString(), addDecimals(100000), 'random wallet two balance should be 100000 tokens after twenty four months');
        let randomWalletTwoLockedBalancesAfterTwentyFourMonths = await contractInstance.getLockedBalanceOf(randomWalletTwo);
        let detailedRandomWalletTwoLockedBalancesAfterTwentyFourMonths = await contractInstance.getDetailedLockedBalanceOf(randomWalletTwo);
        assert.equal(randomWalletTwoLockedBalancesAfterTwentyFourMonths.toString(), addDecimals(0), 'random wallet two total locked amount should be 0 tokens');
        vestedAmounts = getVestedAmounts(detailedRandomWalletTwoLockedBalancesAfterTwentyFourMonths[0], now);
        assert.equal(vestedAmounts.lockedAmount.toString(), addDecimals(0), 'random wallet two locked amount should be 0 tokens after twenty four months');
        assert.equal(vestedAmounts.amountReadyToBeUnlocked.toString(), addDecimals(0), 'random wallet two ready to be unlocked amount should be 0 tokens after twenty four months');

        // Burn and fees
        totalBurnAmount = await contractInstance.totalBurnAmount();
        assert.equal(totalBurnAmount.toString(), addDecimals(0), 'total burn amount should be 0 tokens');
        balanceOfFeeWalletAfterTransaction = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWalletAfterTransaction.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');
    });

    it('regular transactions', async function () {
        let contractInstance = await BornToBeFreeToken.deployed();

        let totalBurnAmountBefore = await contractInstance.totalBurnAmount();
        assert.equal(totalBurnAmountBefore.toString(), addDecimals(0), 'burned token count should be 0');
        let totalBalanceOfFeeWalletBefore = await contractInstance.balanceOf(feeWallet);
        assert.equal(totalBalanceOfFeeWalletBefore.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');

        await contractInstance.transfer(randomWalletOne, web3.utils.toBN(addDecimals(100000)), { from: randomWalletTwo });
        let balanceOfRandomWalletTwo = await contractInstance.balanceOf(randomWalletTwo);
        assert.equal(balanceOfRandomWalletTwo.toString(), addDecimals(0), 'random wallet two balance should be 0 tokens');

        await contractInstance.transfer(randomWalletOne, web3.utils.toBN(addDecimals(2000000)), { from: randomWalletThree });
        let balanceOfRandomWalletThree = await contractInstance.balanceOf(randomWalletThree);
        assert.equal(balanceOfRandomWalletThree.toString(), addDecimals(0), 'random wallet three balance should be 0 tokens');

        await contractInstance.transfer(randomWalletOne, web3.utils.toBN(addDecimals(1800000)), { from: randomWalletFour });
        let balanceOfRandomWalletFour = await contractInstance.balanceOf(randomWalletFour);
        assert.equal(balanceOfRandomWalletFour.toString(), addDecimals(0), 'random wallet four balance should be 0 tokens');

        await contractInstance.transfer(randomWalletOne, web3.utils.toBN(addDecimals(1500000)), { from: teamWallet });
        let balanceOfTeamWallet = await contractInstance.balanceOf(teamWallet);
        assert.equal(balanceOfTeamWallet.toString(), addDecimals(0), 'team wallet balance should be 0 tokens');

        await contractInstance.transfer(randomWalletOne, web3.utils.toBN(addDecimals(1500000)), { from: marketingWallet });
        let balanceOfMarketingWallet = await contractInstance.balanceOf(marketingWallet);
        assert.equal(balanceOfMarketingWallet.toString(), addDecimals(0), 'marketing wallet balance should be 0 tokens');

        await contractInstance.transfer(randomWalletOne, web3.utils.toBN(addDecimals(1000000)), { from: communityWallet });
        let balanceOfCommunityWallet = await contractInstance.balanceOf(communityWallet);
        assert.equal(balanceOfCommunityWallet.toString(), addDecimals(0), 'community wallet balance should be 0 tokens');

        // 1600000 + 100000 + 2000000 + 1800000 + 1500000 + 1500000 + 1000000 = 9500000
        let balanceOfRandomWalletOne = await contractInstance.balanceOf(randomWalletOne);
        assert.equal(balanceOfRandomWalletOne.toString(), addDecimals(9500000), 'random wallet one balance should be 9500000 tokens');

        let totalBurnAmountAfter = await contractInstance.totalBurnAmount();
        assert.equal(totalBurnAmountBefore.toString(), totalBurnAmountAfter.toString(), 'no tokens should be burned');
        let totalBalanceOfFeeWalletAfter = await contractInstance.balanceOf(feeWallet);
        assert.equal(totalBalanceOfFeeWalletAfter.toString(), totalBalanceOfFeeWalletBefore.toString(), 'fee wallet balance should not change');
    });

    it('buy', async function () {
        let contractInstance = await BornToBeFreeToken.deployed();

        let totalBurnAmount = await contractInstance.totalBurnAmount();
        assert.equal(totalBurnAmount.toString(), addDecimals(0), 'total burn amount should be 0 tokens')

        let tokensToTransfer = addDecimals(500000);
        await contractInstance.transfer(randomWalletOne, web3.utils.toBN(tokensToTransfer), { from: pancake });
        let balanceOfPancake = await contractInstance.balanceOf(pancake);
        assert.equal(balanceOfPancake.toString(), addDecimals(0), 'pancake balance should be 0 tokens');

        totalBurnAmount = await contractInstance.totalBurnAmount();
        let taxAndBurnAmountForTransaction = getTaxAndBurnAmounts(BigInt(tokensToTransfer), BUY, BigInt(totalBurnAmount.toString()));
        assert.equal(taxAndBurnAmountForTransaction.burnAmount, BigInt(addDecimals(4000)), 'burn amount for transaction should be 4000 tokens');
        assert.equal(totalBurnAmount.toString(), addDecimals(4000), 'total burn amount should be 4000 tokens');

        let balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        assert.equal(taxAndBurnAmountForTransaction.taxAmount, BigInt(addDecimals(16000)), 'fee for transaction should be 16000 tokens');
        assert.equal(balanceOfFeeWallet.toString(), addDecimals(16000), 'fee wallet balance should be 16000 tokens');

        let balanceOfRandomWalletOne = await contractInstance.balanceOf(randomWalletOne);
        assert.equal(balanceOfRandomWalletOne.toString(), addDecimals(9980000), 'random wallet one balance should be 9980000 tokens');
    });

    it('sell', async function () {
        let contractInstance = await BornToBeFreeToken.deployed();

        let totalBurnAmount = await contractInstance.totalBurnAmount();
        assert.equal(totalBurnAmount.toString(), addDecimals(4000), 'total burn amount should be 4000 tokens');

        let tokensToTransfer = addDecimals(9980000);
        await contractInstance.transfer(pancake, web3.utils.toBN(tokensToTransfer), { from: randomWalletOne });
        let balanceOfRandomWalletOne = await contractInstance.balanceOf(randomWalletOne);
        assert.equal(balanceOfRandomWalletOne.toString(), addDecimals(0), 'random wallet one balance should be 0 tokens');

        totalBurnAmount = await contractInstance.totalBurnAmount(); // 119760 + 4000
        let taxAndBurnAmountForTransaction = getTaxAndBurnAmounts(BigInt(tokensToTransfer), SELL, BigInt(totalBurnAmount.toString()));
        assert.equal(taxAndBurnAmountForTransaction.burnAmount, BigInt(addDecimals(119760)), 'burn amount for transaction should be 119760 tokens');
        assert.equal(totalBurnAmount.toString(), addDecimals(123760), 'total burn amount should be 123760 tokens');

        let balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet); // 479040 + 16000
        assert.equal(taxAndBurnAmountForTransaction.taxAmount, BigInt(addDecimals(479040)), 'fee for transaction should be 479040 tokens');
        assert.equal(balanceOfFeeWallet.toString(), addDecimals(495040), 'fee wallet balance should be 495040 tokens');

        let balanceOfPancake = await contractInstance.balanceOf(pancake);
        assert.equal(balanceOfPancake.toString(), addDecimals(9381200), 'pancake balance should be 9381200 tokens');
    });

    it('burn', async function () {
        let contractInstance = await BornToBeFreeToken.deployed();

        let totalBurnAmountSum = BigInt(addDecimals(123760));

        // Improvisation of burning
        while (totalBurnAmountSum <= BigInt(addDecimals(5000000))) {
            let balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
            await contractInstance.transfer(pancake, balanceOfFeeWallet, { from: feeWallet });

            let balanceOfPancakeWallet = await contractInstance.balanceOf(pancake);
            await contractInstance.transfer(randomWalletOne, balanceOfPancakeWallet, { from: pancake });
            let taxAndBurnAmountAfterTransaction = getTaxAndBurnAmounts(balanceOfPancakeWallet.toString(), BUY, BigInt(0));
            totalBurnAmountSum += BigInt(taxAndBurnAmountAfterTransaction.burnAmount);

            balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
            await contractInstance.transfer(randomWalletOne, balanceOfFeeWallet, { from: feeWallet });

            let balanceOfRandomWalletOne = await contractInstance.balanceOf(randomWalletOne);
            await contractInstance.transfer(pancake, balanceOfRandomWalletOne, { from: randomWalletOne });
            taxAndBurnAmountAfterTransaction = getTaxAndBurnAmounts(balanceOfRandomWalletOne.toString(), SELL, BigInt(0));
            totalBurnAmountSum += BigInt(taxAndBurnAmountAfterTransaction.burnAmount);
        }

        let totalBurnAmount = await contractInstance.totalBurnAmount();
        assert(totalBurnAmount.toString() <= addDecimals(5000000), 'total burn amount should stop at 5`000`000');
        assert.equal(totalBurnAmount.toString(), addDecimals(5000000), 'total burn amount is not 5`000`000');

        let balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        await contractInstance.transfer(pancake, balanceOfFeeWallet, { from: feeWallet });
        balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWallet.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');

        let balanceOfPancakeWallet = await contractInstance.balanceOf(pancake);
        await contractInstance.transfer(randomWalletOne, balanceOfPancakeWallet, { from: pancake });
        balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        let taxAndBurnAmountAfterBuy = getTaxAndBurnAmounts(balanceOfPancakeWallet.toString(), BUY, BigInt(addDecimals(5000000)));
        assert.equal(balanceOfFeeWallet.toString(), taxAndBurnAmountAfterBuy.taxAmount, 'fee wallet balance should be same as total buy tax amount');

        await contractInstance.transfer(randomWalletOne, balanceOfFeeWallet, { from: feeWallet });
        balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWallet.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');

        let balanceOfRandomWalletOne = await contractInstance.balanceOf(randomWalletOne);
        await contractInstance.transfer(pancake, balanceOfRandomWalletOne, { from: randomWalletOne });
        let taxAndBurnAmountAfterSell = getTaxAndBurnAmounts(balanceOfRandomWalletOne.toString(), SELL, BigInt(addDecimals(5000000)));
        balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWallet.toString(), taxAndBurnAmountAfterSell.taxAmount, 'fee wallet balance should be same as total sell tax amount');
    });

    it('tax decrease', async function () {
        let contractInstance = await BornToBeFreeToken.deployed();

        try {
            await contractInstance.setSellTaxPercentage(1, { from: randomWalletTwo });
            assert.fail('Should have failed to change sell tax from random wallet');
        } catch(err) {
            assert.include(err.message, 'revert', 'Error message contains `revert`');
        }

        try {
            await contractInstance.setBuyTaxPercentage(1, { from: randomWalletThree });
            assert.fail('Should have failed to change buy tax from random wallet');
        } catch(err) {
            assert.include(err.message, 'revert', 'Error message contains `revert`');
        }

        let balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        await contractInstance.transfer(pancake, balanceOfFeeWallet, { from: feeWallet });
        balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWallet.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');

        await contractInstance.setBuyTaxPercentage(0);
        let buyTaxPercentage = await contractInstance.buyTaxPercentage();
        assert.equal(buyTaxPercentage, 0, 'buy tax percentage should be 0 percent');

        await contractInstance.setSellTaxPercentage(3);
        let sellTaxPercentage = await contractInstance.sellTaxPercentage();
        assert.equal(sellTaxPercentage, 3, 'sell tax percentage should be 3 percent');

        let balanceOfPancakeWallet = await contractInstance.balanceOf(pancake);
        await contractInstance.transfer(randomWalletOne, balanceOfPancakeWallet, { from: pancake });
        balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWallet.toString(), addDecimals(0), 'fee wallet balance should be zero');
        let taxAndBurnAmountAfterBuy = getTaxAndBurnAmounts(balanceOfPancakeWallet.toString(), BigInt(0), BigInt(addDecimals(5000000)));
        assert.equal(taxAndBurnAmountAfterBuy.taxAmount, addDecimals(0), 'calculated tax amount should be zero');
        let balanceOfRandomWalletOne = await contractInstance.balanceOf(randomWalletOne);
        assert.equal(balanceOfPancakeWallet.toString(), balanceOfRandomWalletOne.toString(), 'random wallet balance should be 100% transfer of pancake wallet balance');

        await contractInstance.transfer(pancake, addDecimals(100000), { from: randomWalletOne });
        balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWallet.toString(), addDecimals(3000), 'fee wallet balance should be 3000 tokens (3% of 100000)');
        taxAndBurnAmountAfterBuy = getTaxAndBurnAmounts(BigInt(addDecimals(100000)), BigInt(3), BigInt(addDecimals(5000000)));
        assert.equal(taxAndBurnAmountAfterBuy.taxAmount, addDecimals(3000), 'calculated tax amount should 3000 tokens');

        await contractInstance.transfer(pancake, balanceOfFeeWallet, { from: feeWallet });
        balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWallet.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');

        await contractInstance.setSellTaxPercentage(0);
        sellTaxPercentage = await contractInstance.sellTaxPercentage();
        assert.equal(sellTaxPercentage, 0, 'sell tax percentage should be 0 percent');

        await contractInstance.transfer(pancake, addDecimals(100000), { from: randomWalletOne });
        balanceOfFeeWallet = await contractInstance.balanceOf(feeWallet);
        assert.equal(balanceOfFeeWallet.toString(), addDecimals(0), 'fee wallet balance should be 0 tokens');
        taxAndBurnAmountAfterBuy = getTaxAndBurnAmounts(BigInt(addDecimals(100000)), BigInt(0), BigInt(addDecimals(5000000)));
        assert.equal(taxAndBurnAmountAfterBuy.taxAmount, addDecimals(0), 'calculated tax amount should 0 tokens');

        try {
            await contractInstance.setSellTaxPercentage(1);
            assert.fail('Should have failed to set sell tax higher');
        } catch(err) {
            assert.include(err.message, 'revert', 'Error message contains `revert`');
        }

        try {
            await contractInstance.setBuyTaxPercentage(1);
            assert.fail('Should have failed to set buy tax higher');
        } catch(err) {
            assert.include(err.message, 'revert', 'Error message contains `revert`');
        }
    });

    it('balance accounts', async function () {
        let contractInstance = await BornToBeFreeToken.deployed();

        let balanceAccounts = await contractInstance.getBalanceAccounts();
        assert.equal(balanceAccounts.length, 4, 'balance accounts should consist of four wallets');

        let addressListOne = [randomWalletOne, randomWalletTwo, randomWalletThree, randomWalletFour];
        assert.equal(addressListOne.every(address => balanceAccounts.includes(address)), true, 'all wallets should be found');

        let addressListTwo = [randomWalletOne, randomWalletTwo, randomWalletThree, randomWalletFour, feeWallet];
        assert.equal(addressListTwo.every(address => balanceAccounts.includes(address)), false, 'fee wallet should not be found');
    });
});