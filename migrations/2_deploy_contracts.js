require('dotenv').config();
const BornToBeFreeToken = artifacts.require('BornToBeFreeToken');

module.exports = async function(deployer, network) {
    try {
        if (network === 'development') {
            let accounts = await web3.eth.getAccounts();
            await deployer.deploy(
                BornToBeFreeToken,
                accounts[1],
                accounts[2],
                accounts[3],
                accounts[4],
                accounts[5],
                accounts[6],
                accounts[7],
                accounts[8],
                accounts[8],
                accounts[8],
            );
        }

        if (network === 'bscTestnet') {
            await deployer.deploy(
                BornToBeFreeToken,
                process.env['TESTNET_FEE_WALLET_PUBLIC_KEY'],
                process.env['TESTNET_COMMUNITY_WALLET_PUBLIC_KEY'],
                process.env['TESTNET_DEVELOPMENT_WALLET_PUBLIC_KEY'],
                process.env['TESTNET_TEAM_WALLET_PUBLIC_KEY'],
                process.env['TESTNET_ANGEL_ROUND_WALLET_PUBLIC_KEY'],
                process.env['TESTNET_PRESALE_ONE_WALLET_PUBLIC_KEY'],
                process.env['TESTNET_PRESALE_TWO_WALLET_PUBLIC_KEY'],
                process.env['TESTNET_PANCAKESWAP_ROUTER_ADDRESS'],
                process.env['TESTNET_PANCAKESWAP_FACTORY_ADDRESS'],
                process.env['TESTNET_BUSD_ADDRESS'],
            );
        }

        if (network === 'bscMainnet') {
            await deployer.deploy(
                BornToBeFreeToken,
                process.env['MAINNET_FEE_WALLET_PUBLIC_KEY'],
                process.env['MAINNET_COMMUNITY_WALLET_PUBLIC_KEY'],
                process.env['MAINNET_DEVELOPMENT_WALLET_PUBLIC_KEY'],
                process.env['MAINNET_TEAM_WALLET_PUBLIC_KEY'],
                process.env['MAINNET_ANGEL_ROUND_WALLET_PUBLIC_KEY'],
                process.env['MAINNET_PRESALE_ONE_WALLET_PUBLIC_KEY'],
                process.env['MAINNET_PRESALE_TWO_WALLET_PUBLIC_KEY'],
                process.env['MAINNET_PANCAKESWAP_ROUTER_V2_ADDRESS'],
                process.env['MAINNET_PANCAKESWAP_FACTORY_V2_ADDRESS'],
                process.env['MAINNET_BUSD_ADDRESS'],
            );
        }
    } catch(e) {
        console.log('Exception:', e);
    }
};
