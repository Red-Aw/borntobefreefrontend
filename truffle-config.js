require('dotenv').config();
const HDWalletProvider = require('@truffle/hdwallet-provider');

module.exports = {
   	contracts_build_directory: './client/src/contracts',

	networks: {
		development: {
			host: "127.0.0.1",
			port: 7545,
			network_id: "*"
		},
		bscTestnet: {
			provider: () => new HDWalletProvider(
				process.env['TESTNET_PRIVATE_KEY'],
				'https://data-seed-prebsc-1-s1.binance.org:8545/'
			),
			network_id: 97,
			skipDryRun: true
		},
		bscMainnet: {
			provider: () => new HDWalletProvider(
				process.env['MAINNET_PRIVATE_KEY'],
				'https://bsc-dataseed.binance.org/'
			),
			network_id: 56,
			confirmations: 10,
			timeoutBlocks: 200,
			skipDryRun: true
		}
	},

	mocha: {
		// timeout: 100000
	},

	compilers: {
		solc: {
			version: '0.8.19',
		}
	}
};
