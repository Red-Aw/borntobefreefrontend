# Contracts For BornToBeFree.io

## Technologies used

[Truffle suite docs](https://trufflesuite.com/docs/)

[Open Zeppelin](https://docs.openzeppelin.com/contracts/4.x/)

---

## Setup

1. Install dependencies: `npm install`
2. Set up environment variable file: `cp .env.dev .env` and insert necessary variables
    - testnet ones are with prefix `TESTNET_`
    - mainnet ones are with prefix `MAINNET_`
3. Run truffle commands

---

## Truffle commands

**Compile** `truffle compile`

**Deployment locally**
- `truffle develop`
- `truffle migrate --reset`

**Deployment with provided network**
- `truffle migrate --reset --network bscTestnet`
- `truffle migrate --reset --network bscMainnet`
- `truffle migrate --network bscTestnet`
- `truffle migrate --network bscMainnet`

**Truffle console with provided network**
- `truffle console --network dashboard`

**Truffle run scripts**
- `truffle exec scripts/[script-name].js --network [networkName]`

**Truffle develop and test locally**
- `ganache-cli -p 7545 -a 15`
- `truffle console --network development`
- `truffle migrate --reset --network development`
- `truffle test --network development`

---

## Deployment sequence to mainnet
1. Deploy contract
2. Add liquidity in [PancakeSwap](https://pancakeswap.finance/)
3. Lock liquidity lp tokens via [Mudra Locker](https://www.mudra.website/)
    - Check [Poo Coins](https://poocoin.app/)
4. Run initialization function on BTBF contract
