const BornToBeFreeToken = artifacts.require('BornToBeFreeToken');

module.exports = async (initialize) => {
    // Gets BTBF token contract instance
    let contractInstance = await BornToBeFreeToken.deployed();
    console.log(contractInstance.address); // Must be the same as deployed address

    // Runs initialize
    await contractInstance.initialize();

    initialize();
};