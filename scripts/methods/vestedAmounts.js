const ethers = require('ethers');

module.exports.getVestedAmounts = function(list) {
    let lockedAmount = BigInt(0);
    let amountReadyToBeUnlocked = BigInt(0);
    let nextUnlockDateTime = 0;

    let blockchainTimestamp = Math.round(Date.now() / 1000) - 180; // minus 3 minutes

    list.forEach(lockedStructure => {
        if (lockedStructure.amount === '0') {
            return;
        }

        if (nextUnlockDateTime === 0 && lockedStructure.releaseTimestamp > blockchainTimestamp) {
            nextUnlockDateTime = lockedStructure.releaseTimestamp;
        }

        if (lockedStructure.releaseTimestamp > blockchainTimestamp) {
            lockedAmount += BigInt(lockedStructure.amount);
        } else {
            amountReadyToBeUnlocked += BigInt(lockedStructure.amount);
        }
    });

    return {
        'lockedAmount': ethers.formatEther(lockedAmount),
        'amountReadyToBeUnlocked': ethers.formatEther(amountReadyToBeUnlocked),
        'nextUnlock': nextUnlockDateTime,
    };
}