const BornToBeFreeToken = artifacts.require('BornToBeFreeToken');

module.exports = async (releaseLockedAmounts) => {
    try {
        let contractInstance = await BornToBeFreeToken.deployed();

        let balanceAccountList = await contractInstance.getBalanceAccounts();

        for (const address of balanceAccountList) {
            await contractInstance.releaseAmountsFor(address);
        }
    } catch(e) {
        console.log('Exception:', e);
    }

    releaseLockedAmounts();
};
