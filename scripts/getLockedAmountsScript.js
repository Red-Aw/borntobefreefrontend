const BornToBeFreeToken = artifacts.require('BornToBeFreeToken');
const ethers = require('ethers');
const Methods = require('./methods/vestedAmounts.js')

module.exports = async (getLockedAmounts) => {
    try {
        let contractInstance = await BornToBeFreeToken.deployed();

        // Provide address here
        let address = '0x02E16C5AA955574D261C0C3fb89e28D5bD3d6dC6';

        let balanceOfResult = await contractInstance.balanceOf(address);
        let lockedBalanceOfResult = await contractInstance.getLockedBalanceOf(address);
        let detailedLockedBalanceOfResult = await contractInstance.getDetailedLockedBalanceOf(address);
        console.log('Address: ' + address);

        console.log('Balances in wallet: ' + ethers.formatEther(balanceOfResult.toString()));

        console.log('Vested amounts:');
        console.log('Total locked amounts: ' + ethers.formatEther(lockedBalanceOfResult.toString()));

        console.log('Angel round:');
        let angelRoundResult = Methods.getVestedAmounts(detailedLockedBalanceOfResult[0]);
        console.log(angelRoundResult);

        console.log('PreSale #1:');
        let preSaleOneResult = Methods.getVestedAmounts(detailedLockedBalanceOfResult[1]);
        console.log(preSaleOneResult);

        console.log('PreSale #2:');
        let preSaleTwoResult = Methods.getVestedAmounts(detailedLockedBalanceOfResult[2]);
        console.log(preSaleTwoResult);

        console.log('Marketing tokens:');
        let marketingTokenResult = Methods.getVestedAmounts(detailedLockedBalanceOfResult[3]);
        console.log(marketingTokenResult);

        console.log('Team tokens:');
        let teamTokenResult = Methods.getVestedAmounts(detailedLockedBalanceOfResult[4]);
        console.log(teamTokenResult);
    } catch(e) {
        console.log('Exception:', e);
    }

    getLockedAmounts();
};
