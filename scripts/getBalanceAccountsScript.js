const BornToBeFreeToken = artifacts.require('BornToBeFreeToken');

module.exports = async (getAllWallets) => {
    let contractInstance = await BornToBeFreeToken.deployed();

    let balanceAccountList = await contractInstance.getBalanceAccounts();
    console.log(balanceAccountList);

    getAllWallets();
};