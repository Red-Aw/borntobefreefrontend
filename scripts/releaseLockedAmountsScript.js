const BornToBeFreeToken = artifacts.require('BornToBeFreeToken');

module.exports = async (releaseLockedAmounts) => {
    try {
        let contractInstance = await BornToBeFreeToken.deployed();

        // Provide address here
        let address = '0x02E16C5AA955574D261C0C3fb89e28D5bD3d6dC6';

        await contractInstance.releaseAmountsFor(address);
    } catch(e) {
        console.log('Exception:', e);
    }

    releaseLockedAmounts();
};
